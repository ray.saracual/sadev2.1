<?php
include"../conexion/sesion.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>SADEV2.1 | Patrulla Sectorial</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.theme.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.css">


<!-- Latest compiled and minified JavaScript -->
<script src="../js/jquery-1.11.1.min.js"></script>
   <script src="../js/jquery-ui.min.js"></script>
     <script src="../dist/js/bootstrap.js"></script>
      <script src="../jui/jquery-ui-1.11.2.custom/jquery-ui.min.js"></script>
      
    

<style>
#container{
	margin-left:auto;
	margin-right:auto;
	
	
	}
#select_dependiente {
	
	margin-left:3%;
	margin-bottom:3%;

	border-radius:5%;
box-shadow : rgba(0,0,0,0.3) 0px 0px 1em;

width:auto;

	}
.datos_complementarios {
	margin-left:5%;
	margin-right:auto;
	box-shadow : rgba(0,0,0,0.3) 0px 0px 1em;	
	margin-bottom:5%;
	width:90%;
	
}
	.datos_complementarios label {
				
		
		}
	#select_dependiente ,.datos_complementarios{
		display: inline-block;
		vertical-align:top;
			
		
		}		
		
		
		
		.titulo_iniciarS{
		
		background-color:#900; 
		height:30px; 
		padding-top:5px; 
		color:#FFF;
		text-align:center;
		width:100%;
			
		
		}

</style>

<script language="javascript">
$(document).ready(function(){
	
   
    $( "#mensaje" ).dialog({
      hide:"explode",
      modal: true,

  });
  
  var nav4 = window.Event ? true : false;
function acceptNum(evt){ 
// NOTE: Backspace = 8, Enter = 13, '0' = 48, '9' = 57 
var key = nav4 ? evt.which : evt.keyCode; 
return (key <= 13 || (key >= 48 && key <= 57));

}
   
});

</script>

<?php
include "../conexion/db.php";

// Conexión a la base de datos

$enlace  = conectar();

   ?>

</head>



<body >

<?php 

include ("../menu/menu.php");
?>


<ol class="breadcrumb">
  <li><a href="#">Inicio</a></li>
  <li class="active">BUSCAR PATRULLA SECTORIAL POR CÉDULA DEL JEFE</li>
</ol>
<h4 align="center"> SELECCIONA UNA OPCIÓN DEL MENÚ SUPERIOR </h4>

<!--<div class="jumbotron" style="margin:2%;">
  <h1 style=" padding:2%">Listados </h1>
  
  
<p style=" padding-left:2%">
Listados por Circulo de lucha del Buen Vivir
<span class="glyphicon glyphicon-list-alt" > </span>
</p>
<p style=" padding-left:2%">
  <a href="buscar_p_sectorial.php">Listados por Unidad de Batalla Bolivar Chavez </a> 
  <span class="glyphicon glyphicon-list-alt" > </span></p>
  <!--<p style=" padding:2%"><a class="btn btn-danger" href="#" role="button">Seleccione una opción del menú superior.</a></p>-->
</div>


<div id="mensaje" title="INGRESE LOS DATOS...">
<form id="form1" name="form1" action="?" method="POST" >

   


<div class="form-group">
<label> Ingrese la cédula del Jefe de Patrulla Sectorial  <span class="obligatorio">*</span></label>
<input type="number" name="cedula" id="cedula" placeholder="CEDULA JEFE DE PATRULLA" class="form-control" required onKeyPress="return acceptNum(event)">	

</div>







        
<div class="bg-danger" align="center">
<button type="reset" name="restablecer" class="btn-danger" >
Restablecer  <span class="glyphicon glyphicon-refresh" aria-hidden="true">
</span></button>

<button type="submit" name="registrar" class=" btn-success" >
Procesar   <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true">
</span></button>
</div>


</form><!-- FIN FORMULARIO reg_jefe_circulo-->


</div><!-- FIN DIALOG O DIV MENSAJE -->

<div class="datos_complementarios"> 

 <?php 
		/**************************** ACA OBTENGO JEFE DE PATRULLA SECTORIAL***********************************/
		if(isset($_POST["cedula"])){

$datos_patrulla = mysql_query("SELECT * FROM tbl_jefe_psectoriales AS j_sec JOIN tbl_p_sectoriales AS p_sec
ON j_sec.id_p_sectorial= p_sec.id_p_sectorial 
JOIN tbl_centros_2014_clp AS centros_2014 ON j_sec.codigo_centro = centros_2014.codigo_centro
WHERE cedula=$_REQUEST[cedula]",$enlace);

$totalRows_p_sectorial = mysql_num_rows ($datos_patrulla);




	
	if($totalRows_p_sectorial>0){?>
    <p class="titulo_iniciarS">
                    JEFE DE PATRULLA SECTORIAL
                  </p>
    	<div   style="padding-left:0.5%; padding-right:0.5%;"> 
<table class="table table-bordered">
    <thead>
        <tr class="active">
            <th >Nombre de UBCH </th>
            <th>Patrulla Sectorial </th>
            <th>Nombre y Apellido</th>
            <th>Cédula</th>
             <th>Télefono</th>
             
        </tr>
    </thead>  
    <?php
	$cont=0;
	if($row=mysql_fetch_array($datos_patrulla)){
									
         
		  ?>
    <tbody>
        <tr class="brillo">
            <td><?php echo $row["nombre_centro"] ?></td>
             <td><?php echo $row["p_sectorial"] ?></td>
            <td><?php echo $row["nombre_apellido"] ?></td>
            <td><?php echo $row["cedula"] ?></td>
            <td><?php echo $row["telefono"] ?></td>
        </tr>
         </tbody>
</table>
		  <?php
								
	}}else {  ?><!--- CIERRO WHILE DE JEFES DE PATRULLAS SECTORIALES Y IF CUANDO TOTAL ROW JEFE ES MAYOR A 0 "CERO"----------------->		   
     <p class="titulo_iniciarS">
                   PATRULLEROS 
                  </p>
            <p align="center">NO HAY REGISTROS.</p>



 <?php }} ?>
 
 
 
  <?php 
     
/**************************** ACA OBTENGO PATRULLEROS SECTORIALES DEL JEFE SEC********************************/
		
if(isset($row["codigo_centro"])&&$row["id_p_sectorial"]){
$datos_patrulleros = mysql_query("SELECT * FROM  tbl_patrulleros_sec AS p_sec 
WHERE codigo_centro=$row[codigo_centro] AND id_p_sectorial=$row[id_p_sectorial] ",$enlace);

$totalRows_patrullero= mysql_num_rows ($datos_patrulleros);

	
	if($totalRows_patrullero>0){?>
    <p class="titulo_iniciarS">
                   PATRULLEROS 
                  </p>
    	<div   style="padding-left:0.5%; padding-right:0.5%;"> 
<table class="table table-bordered">
    <thead>
        <tr class="active">
            <th>#</th>
           <th>Nombre y Apellido</th>
            <th>Cédula</th>
             <th>Télefono</th>
              <th>Administar</th>
        </tr>
    </thead>  
    <?php
	$cont=0;
	while($row=mysql_fetch_array($datos_patrulleros)){
		$cont++;							
         
		  ?>
   
       <tbody>
        <tr class="brillo"> 
             <td><?php echo $cont; ?></td>
            <td><?php echo $row["nombre_apellido"] ?></td>
            <td><?php echo $row["cedula"] ?></td>
             <td><?php echo $row["telefono"] ?></td>
                <td  align="center"> <a class="btn btn-mini" href="edit_patrullero_sec.php?codigo=<?php 
				echo $row['id_patrullero_sec']; ?>">
                <span class="glyphicon glyphicon-pencil"></span>
               </a>
                 <?php if ($_SESSION['eliminar']==1){?>   
               <a class="btn btn-mini" href="eli_patrullero_sec.php?codigo=<?php echo $row['id_patrullero_sec']; ?>">
                <span class=" glyphicon glyphicon-trash"></span>
              </a>
               <?php } ?>
               </td>
     
        </tr>
        </tbody>
		  <?php
								
	}}else {  ?><!--- CIERRO WHILE DE JEFES DE PATRULLAS SECTORIALES Y IF CUANDO TOTAL ROW JEFE ES MAYOR A 0 "CERO"----------------->	
  
           <p class="titulo_iniciarS">
                   PATRULLEROS 
                  </p> 
           <p align="center">NO HAY REGISTROS.</p>



 <?php }} ?>
    
 </tbody>
</table>
 
 
 </div><!--FIN DATOS COMPLEMENTARIOS ---->
 

 
    </tbody>
</table>

  <div style="text-align:center;">
<?php if (isset($totalRows_p_sectorial)>0){?>
 <a class="btn btn-mini" href="reportes/patrulla_sec_prinf.php?cedula=<?php echo $_REQUEST['cedula']; ?>" target="new">
                <span class="glyphicon glyphicon-print"> IMPRIMIR</span>
              </a>
</div>
<?php } ?>
</div><!--- FIN TABLE RESPONSIVE-------->
 
</body>
</html>