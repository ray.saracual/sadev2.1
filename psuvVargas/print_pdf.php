<?php require_once('file:///C|/Connections/sadev.php'); ?>
<?php
	mysql_select_db($database_sadev, $sadev);
	$query_CENTRO = "SELECT P.nombre AS parroquia, C.centro_electoral AS nombre_centro, C.cod_centro_electoral AS codigo, J.nombres AS co_elec_nombre, J.apellidos AS co_elec_apellido, J.cedula AS co_elec_cedula, J.telefonos_1 AS co_elec_telefono, J2.nombres AS co_ubb_nombre, J2.apellidos AS co_ubb_apellido, J2.cedula AS co_ubb_cedula, J2.telefonos_1 AS co_ubb_telefono
		FROM alcaldia_bp_centros_mesas_vargas AS C
		LEFT JOIN parroquia AS P ON (P.id=C.id_parroquia)
		LEFT JOIN alcaldia_bp_estructura AS J ON (C.cod_centro_electoral=J.cod_centro_bp and J.id_cargo_bp='4')
		LEFT JOIN alcaldia_bp_estructura AS J2 ON (C.cod_centro_electoral=J2.cod_centro_bp and J2.id_cargo_bp='1')
		WHERE C.cod_centro_electoral='$_REQUEST[id_centro]'";
	$CENTRO = mysql_query($query_CENTRO, $sadev) or die(mysql_error());
	$row_CENTRO = mysql_fetch_assoc($CENTRO);
	$totalRows_CENTRO = mysql_num_rows($CENTRO);

	mysql_select_db($database_sadev, $sadev);
	$query_LST = "SELECT T.mesa, T.cargo, T.nombre, T.nacionalidad, T.cedula, T.telefono, IF(T.experiencia='SI','Si','No') AS experiencia, IF(T.adiestrado='1','Si','No') AS adiestrado
		FROM alcaldia_bp_testigo AS T
		WHERE T.cod_centro='$_REQUEST[id_centro]'
		ORDER BY T.mesa ASC, T.id ASC";
	$LST = mysql_query($query_LST, $sadev) or die(mysql_error());
	$row_LST = mysql_fetch_assoc($LST);
	$totalRows_LST = mysql_num_rows($LST);
	
	$_SESSION['parroquia'] = $row_CENTRO[parroquia];
	$_SESSION['nombre_centro'] = $row_CENTRO[nombre_centro];
	$_SESSION['codigo'] = $row_CENTRO[codigo];
	$_SESSION['co_elec_nombre'] = strtoupper(utf8_decode($row_CENTRO[co_elec_nombre].' '.$row_CENTRO[co_elec_apellido]));	
	$_SESSION['co_elec_cedula'] = $row_CENTRO[co_elec_cedula];
	$_SESSION['co_elec_telefono'] = $row_CENTRO[co_elec_telefono];	
	$_SESSION['co_ubb_nombre'] = strtoupper(utf8_decode($row_CENTRO[co_ubb_nombre].' '.$row_CENTRO[co_ubb_apellido]));	
	$_SESSION['co_ubb_cedula'] = $row_CENTRO[co_ubb_cedula];
	$_SESSION['co_ubb_telefono'] = $row_CENTRO[co_ubb_telefono];	
?>
<?php

///////////////////////////////////////////[ CONVERSION A PDF ]///////////////////////////////
require('file:///C|/pdf/fpdf.php');
class PDF extends FPDF{
	function Header(){
		$this->SetLeftMargin(6);
		$this->Image('../../../../images/ccc.jpg',8,8,45,0);
		$this->Image('../../../../images/psuv_small.jpg',246,8,45,0);		
		$this->SetFont('Arial','B',12);
		$this->Cell(0,0,'PARTIDO SOCIALISTA UNIDO DE VENEZUELA',0,0,'C');		
		$this->Ln(5);		
		$this->Cell(0,0,'SALA SITUACIONAL / ESTADO VARGAS',0,0,'C');		
		$this->Ln(5);		
		$this->Cell(0,0,'COMANDO DE CAMPA�A CARABOBO',0,0,'C');		
		$this->Ln(17);		
		$this->SetFont('Arial','B',15);		
		$this->Cell(0,0,'REGISTRO DE TESTIGOS DE MESAS PARA EL D�A 07 DE OCTUBRE 2012',0,0,'C');		
		/* $this->SetFont('Arial','B',10); */
		$this->Ln(10);		
		$this->Cell(0,0,$_SESSION['parroquia'],0,0,'L');				
		$this->Ln(5);
		$this->SetFont('Arial','',10);
		$this->Cell(0,0,'Nombre del Centro de Votaci�n: '.str_replace('                 ','',$_SESSION['nombre_centro']),0,0,'L');
		$this->Ln(5);
		$this->Cell(0,0,'C�digo del Centro: ' .$_SESSION['codigo'],0,0,'L');
		$this->Ln(5);		
		$this->Cell(0,0,'Coordinador de Testigos de la Base de Patrulla: '.$_SESSION['co_elec_nombre'],0,0,'L');
		$this->Ln(5);	
		$this->Cell(0,0,'C�dula: '.$_SESSION['co_elec_cedula'].'    Tel�fono: '.$_SESSION['co_elec_telefono'],0,0,'L');			
		$this->Cell(70);
		$this->Ln(10);
	}
	function Footer()	{
		$this->SetY(-25);
		$this->SetFont('Arial','',10);
		$this->Cell(0,0,'Coordinador de la Base de Patrulla: '.$_SESSION['co_ubb_nombre'].'. C�dula: '.$_SESSION['co_ubb_cedula'].'. Tel�fono: '.$_SESSION['co_ubb_telefono'],0,0,'C');				
		$this->Ln(5);		
		$this->SetFont('Arial','I',7);
		$this->Cell(0,10,'SISTEMA AUTOMATIZADO NACIONAL PARA LA DEFENSA DEL VOTO [SADEV]',0,0,'C');		
		$this->Image('../../../../images/leyenda.jpg',8,180,30,0);		
	}
}

//Creacin del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage(L);

$pdf->SetFillColor(232,232,232);		
$pdf->SetTextColor(0,0,0);	
$pdf->SetFont('Arial','B',8);	
$pdf->Cell(8,5,'Mesa',1,0,'C',1);
$pdf->Cell(46,5,'Testigo Principal',1,0,'C',1);
$pdf->Cell(15,5,'C�dula',1,0,'C',1);
$pdf->Cell(19,5,'Telef�no',1,0,'C',1);
$pdf->Cell(4,5,'E',1,0,'C',1);
$pdf->Cell(4,5,'A',1,0,'C',1);
$pdf->Cell(4,5,'10',1,0,'C',1);
$pdf->Cell(46,5,'1er. Testigo Suplente',1,0,'C',1);
$pdf->Cell(15,5,'C�dula',1,0,'C',1);
$pdf->Cell(19,5,'Telef�no',1,0,'C',1);
$pdf->Cell(4,5,'E',1,0,'C',1);
$pdf->Cell(4,5,'A',1,0,'C',1);
$pdf->Cell(4,5,'10',1,0,'C',1);
$pdf->Cell(46,5,'2do. Testigo Suplente',1,0,'C',1);
$pdf->Cell(15,5,'C�dula',1,0,'C',1);
$pdf->Cell(19,5,'Telef�no',1,0,'C',1);
$pdf->Cell(4,5,'E',1,0,'C',1);
$pdf->Cell(4,5,'A',1,0,'C',1);
$pdf->Cell(4,5,'10',1,0,'C',1);
$pdf->Ln(5);
$i=0;
$pdf->SetFont('Arial','',8);
	
do {
  $i++;
  if ($i==1){
	  $pdf->Cell(8,5,$row_LST[mesa],1,0,'C');
  }
  $pdf->Cell(46,5,ucwords(strtolower(utf8_decode($row_LST[nombre]))),1,0,'L');
  $pdf->Cell(15,5,$row_LST[cedula],1,0,'R');
  $pdf->Cell(19,5,$row_LST[telefono],1,0,'R');
  $pdf->Cell(4,5,'',1,0,'C');
  $pdf->Cell(4,5,'',1,0,'C');
 // $pdf->Cell(4,5,$row_LST[experiencia],1,0,'C');
 // $pdf->Cell(4,5,$row_LST[adiestrado],1,0,'C');
  $pdf->Cell(4,5,'',1,0,'C');
  if ($i==3){
	  $pdf->Ln(5);
	  $i=0;
  }
} while ($row_LST = mysql_fetch_assoc($LST));
$pdf->Output();

mysql_free_result($JP);
mysql_free_result($LST);
?>