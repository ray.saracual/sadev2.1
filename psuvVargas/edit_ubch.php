<?php 
include"../conexion/sesion.php";
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
<meta charset="utf-8">
<title>SADEV2.1 | Editar UBCH </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.theme.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.css">


<style>
#container{
	margin-left:auto;
	margin-right:auto;
	
	
	}
#select_dependiente {
	
	margin-left:3%;
	margin-bottom:3%;

	border-radius:5%;
box-shadow : rgba(0,0,0,0.3) 0px 0px 1em;

width:auto;

	}
.datos_complementarios {
	margin-left:5%;
	margin-right:auto;
	box-shadow : rgba(0,0,0,0.3) 0px 0px 1em;	
	margin-bottom:5%;
	width:90%;
	
}
	.datos_complementarios label {
				
		
		}
	#select_dependiente ,.datos_complementarios{
		display: inline-block;
		vertical-align:top;
		
		}		
		.titulo_iniciarS{
		background-color:#900;
		height:30px; 
		padding-top:5px; 
		color:#FFF;
		text-align:center;
		width:100%;
		}

</style>


<!-- Latest compiled and minified JavaScript -->
<script src="../js/jquery-1.11.1.min.js"></script>
   <script src="../js/jquery-ui.min.js"></script>
     <script src="../dist/js/bootstrap.js"></script>


<script language="javascript">
function nuevoAjax(){
	var xmlhttp=false;
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}catch(E){
			xmlhttp = false;
		}
	}

	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	
	return xmlhttp;
}


function EliminarDato(){
		
	resul = document.getElementById('resultado');
	
	//bus=document.frmbusqueda.nro_acta.value;
	bus=document.form1.cedula.value;
	
	ajax=nuevoAjax();
	ajax.open("POST", "busqueda.php",true);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			resul.innerHTML = ajax.responseText
		}
	}
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send("busqueda="+bus,+combo1,+combo2+combo3)

}


</script>


</head>

<?php 

include "../conexion/db.php";

// Conexión a la base de datos

$enlace  = conectar();
///////////////////////////SELECT PARA ELIMINAR REGISTRO ///////////////////////////////////////////
	
	

///////////////////////////SELECT PARA OBTENER PARROQUIA ///////////////////////////////////////////
	 $parroquia=mysql_query("SELECT * FROM tbl_parroquias AS pq
	 WHERE pq.id_parroquia = $_REQUEST[combo1]",$enlace);
	$totalRows_parroquia = mysql_num_rows ($parroquia);
	$pq=mysql_fetch_array($parroquia);
	
			?>

<body >

<nav id="menu">

<?php include "../menu/menu.php" ?>

</nav>
<!---------------------- FIN MENÚ---------------------------------->


<ol class="breadcrumb">
  <li><a href="#">INICIO</a></li>
  <li class="active">EDITAR UBCH</li>
  <li class="active"><?php echo $pq['parroquia'] ?></li>
  <li class="active">CIRCULO &nbsp; <?php echo $_REQUEST['combo2'] ?></li>
  <li class="active">UBCH &nbsp;<?php echo $_REQUEST['combo3'] ?></li>
</ol>


<div class="datos_complementarios"> 

<p class="titulo_iniciarS">
                   JEFE DE UBCH
                  </p>

	 <?php 
	
	///////////////////////////SELECT PARA OBTENER JEFE UBCH ///////////////////////////////////////////
	$jefe_ubch=mysql_query("SELECT * FROM tbl_jefe_ubch AS j_ubch JOIN tbl_centros_2014_clp AS centros_2014
	ON j_ubch.codigo_centro=centros_2014.codigo_centro
	 WHERE j_ubch.codigo_centro = $_REQUEST[combo3]",$enlace);
	$totalRows_j_ubch = mysql_num_rows ($jefe_ubch);
	
	
	if($totalRows_j_ubch>0){?>
    	<div  style="padding-left:0.5%; padding-right:0.5%;"> 
<table class="table table-bordered">
    <thead>
        <tr class="active">
            <th>CODIGO DE UBCH</th>
            <th>NOMBRE DE UBCH</th>
            <th>JEFE DE UBCH</th>
            <th>CÉDULA</th>
            <th>TÉLEFONO</th>
            <th>ADMINISTRAR      </th>
            
        </tr>
    </thead>  
    <?php
	$cont=0;
	if($row=mysql_fetch_array($jefe_ubch)){
		$cont=$cont+1;								
         
		  ?>
    <tbody>
        <tr class="brillo">
            <td><?php echo $_REQUEST['combo3']  ?></td>
             <td><?php echo $row["nombre_centro"] ?></td>
            <td><?php echo $row["nombre_apellido"] ?></td>
            <td><?php echo $row["cedula"] ?></td>
             <td><?php echo $row["telefono"] ?></td>
              <td align="center"><a class="btn btn-mini" href="edit_j_ubch.php?codigo=<?php echo($row['id_jefe_ubch']); ?>">
                <span class="glyphicon glyphicon-pencil"></span>
               </a>
                 <?php if ($_SESSION['eliminar']==1){?>   
                 <a class="btn btn-mini" href="eli_j_ubch.php?codigo=<?php echo $row['id_jefe_ubch']; ?>">
                <span class=" glyphicon glyphicon-trash"></span>
              </a>
               <?php } ?>
               
               </td>
              
        </tr>
		  <?php
								
}}else {  ?><!--- CIERRO WHILE DE JEFES DE PATRULLAS SECTORIALES Y IF CUANDO TOTAL ROW JEFE ES MAYOR A 0 "CERO"----------------->		   
            <p align="center">NO HAY REGISTROS.</p>

 <?php } ?>
    </tbody>
</table>

<p class="titulo_iniciarS">
                    JEFES DE PATRULLAS SECTORIALES
                  </p>

 <?php 
	///////////////////////////SELECT PARA OBTENER LOS 10 ///////////////////////////////////////////
	$jefe_p_sectorial=mysql_query("SELECT * FROM tbl_jefe_psectoriales AS jp_sectorial JOIN tbl_p_sectoriales AS p_sectorial
	ON jp_sectorial.id_p_sectorial=p_sectorial.id_p_sectorial WHERE codigo_centro = $_REQUEST[combo3]",$enlace);
	$totalRows_p_sectorial = mysql_num_rows ($jefe_p_sectorial);
	
	
	if($totalRows_p_sectorial>0){?>
    	<div   style="padding-left:0.5%; padding-right:0.5%;"> 
<table class="table table-bordered">
    <thead>
        <tr class="active">
            <th>#</th>
            <th>Patrulla Sectorial </th>
            <th>Nombre y Apellido</th>
            <th>Cédula</th>
             <th>Télefono </th>
            <th> Administar </th>
        </tr>
    </thead>  
    <?php
	$cont=0;
	while($row=mysql_fetch_array($jefe_p_sectorial)){
		$cont=$cont+1;								
         
		  ?>
    <tbody>
       <tr class="brillo">
            <td><?php echo $cont ?></td>
             <td><?php echo $row["p_sectorial"] ?></td>
            <td><?php echo $row["nombre_apellido"] ?></td>
            <td><?php echo $row["cedula"] ?></td>
            <td><?php echo $row["telefono"] ?></td>
            
           <td  align="center"> <a class="btn btn-mini" href="edit_j_sectorial.php?codigo=<?php echo $row['id_jefe_psectorial']; ?>">
                <span class="glyphicon glyphicon-pencil"></span>
               </a>
             <?php if ($_SESSION['eliminar']==1){?>    
               <a class="btn btn-mini" href="eli_j_sectorial.php?codigo=<?php echo $row['id_jefe_psectorial']; ?>">
                <span class=" glyphicon glyphicon-trash"></span>
              </a>
                <?php } ?>
               <a class="btn btn-mini" href="ver_patrullas.php?codigo=<?php echo $row['id_jefe_psectorial']; ?>">
                <span class=" glyphicon glyphicon-eye-open"></span>
              </a>
             
               </td>
        </tr>
		  <?php
								
}}else {  ?><!--- CIERRO WHILE DE JEFES DE PATRULLAS SECTORIALES Y IF CUANDO TOTAL ROW JEFE ES MAYOR A 0 "CERO"----------------->		   
            <p align="center">NO HAY REGISTROS.</p>

 <?php } ?>
    </tbody>
</table>
</div><!--- FIN TABLE RESPONSIVE-------->

</div>
</div><!-- FIN DATOS COMPLEMENTARIO-->

</body>
</html>