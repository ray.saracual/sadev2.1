<?php 
include"../conexion/sesion.php";
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>SADEV2.1 |Jefe de Circulo </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.theme.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.css">


<style>
#container{
	margin-left:auto;
	margin-right:auto;
	
	
	}
#select_dependiente {
	
	margin-left:3%;
	margin-bottom:3%;

	border-radius:5%;
box-shadow : rgba(0,0,0,0.3) 0px 0px 1em;

width:auto;
	
	

	}
.datos_complementarios {
	margin-left:3%;
	margin-right:auto;
	box-shadow : rgba(0,0,0,0.3) 0px 0px 1em;
	
	margin-bottom:5%;
	width:70%;
	
	border-radius:3%;

	
		}
	#select_dependiente ,.datos_complementarios{
		display: inline-block;
		vertical-align:top;

		}		
		
		
		
		.titulo_iniciarS{
		border-top-left-radius: 10px;
        border-top-right-radius: 10px; 
		background-color:#900; 
		height:30px; 
		padding-top:5px; 
		color:#FFF;
		text-align:center;
		width:100%;
			
		
		}

</style>





<!-- Latest compiled and minified JavaScript -->
<script src="../js/jquery-1.11.1.min.js"></script>
   <script src="../js/jquery-ui.min.js"></script>
     <script src="../dist/js/bootstrap.js"></script>
 <script src="../jui/jquery-ui-1.11.2.custom/jquery-ui.min.js"></script>

<script language="javascript">
$(document).ready(function(){
	// Parametros para e combo1
   $("#combo1").change(function (valor) {
	    
	   
	    $("#combo1 option:selected").each(function () {
			//alert($(this).val());
				elegido=$(this).val();
				$.post("combo1.php", { elegido: elegido }, function(data){
				$("#combo2").html(data);
			
				
			});
			
        });
		
   })
   
  		
	// Parametros para el combo2
	$("#combo2").change(function () {
   		$("#combo2 option:selected").each(function () {
			 //alert($(this).val());
				elegido=$(this).val();
				$.post("combo2.php", { elegido: elegido }, function(data){
				
			});			
        });
   })
   
    $( "#mensaje" ).dialog({
      hide:"explode",
      modal: true,
	
	  
	  
   
  });
   
   
});




function nuevoAjax(){
	var xmlhttp=false;
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}catch(E){
			xmlhttp = false;
		}
	}

	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	
	return xmlhttp;
}


function buscarDato(){
		
	resul = document.getElementById('resultado');
	
	//bus=document.frmbusqueda.nro_acta.value;
	bus=document.form1.cedula.value;
	
	ajax=nuevoAjax();
	ajax.open("POST", "busqueda.php",true);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			resul.innerHTML = ajax.responseText
		}
	}
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send("busqueda="+bus)

}

function buscarCirculo(){
		
	resul = document.getElementById('resultado_circulo');
	
	//bus=document.frmbusqueda.nro_acta.value;
	bus=document.form1.combo2.value;
	
	ajax=nuevoAjax();
	ajax.open("POST", "busqueda_circulo.php",true);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			resul.innerHTML = ajax.responseText
		}
	}
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send("busqueda="+bus)

}

var nav4 = window.Event ? true : false;
function acceptNum(evt){ 
// NOTE: Backspace = 8, Enter = 13, '0' = 48, '9' = 57 
var key = nav4 ? evt.which : evt.keyCode; 
return (key <= 13 || (key >= 48 && key <= 57));

}

</script>



   

</head>

<?php 
include "../conexion/db.php";

// Conexión a la base de datos

$enlace  = conectar();



///////////////////////////INSERTANDO FORMULARIO///////////////////////////////////////////

if (isset($_POST["cedula"])){
$cedula = $_REQUEST['cedula'];




// SELECT A CEDULA DE DELEGADO PARA VALIDAR//
$delegados_psuv = mysql_query ("SELECT * FROM tbl_delegados AS delegados  
                            WHERE delegados.cedula = '$_REQUEST[cedula]'", $enlace) or
                                 die("Problemas en el select:".mysql_error());
	 				 
  
    $totalRows_delegados = mysql_num_rows($delegados_psuv);
		$delegados= mysql_fetch_array($delegados_psuv );
	
	//SELECT A CEDULA  PARA VALIDAR CON TBL_JEFE_CIRCULO//
$jefe_circulo = mysql_query ("SELECT * FROM tbl_jefe_circulo AS j_circulo 
                             WHERE j_circulo.cedula = '$_REQUEST[cedula]'", $enlace) or
                                 die("Problemas en el select:".mysql_error());
	 				 
  
    $totalRows_j_circulo = mysql_num_rows($jefe_circulo);
		$j_circulo= mysql_fetch_array($jefe_circulo);
	
	// SELECT A CEDULA DE DELEGADO PARA VALIDAR CON TBL_JEFE_UBCH//
$jefe_ubch = mysql_query ("SELECT * FROM tbl_jefe_ubch AS j_ubch 
                             WHERE j_ubch.cedula = '$_REQUEST[cedula]'", $enlace) or
                                 die("Problemas en el select:".mysql_error());
	 				 
  
    $totalRows_j_ubch = mysql_num_rows($jefe_ubch);
		$j_ubch= mysql_fetch_array($jefe_ubch );
		
		// SELECT A CEDULA DE DELEGADO PARA VALIDAR CON TBL_JEFE_PSECTORIAL//
$jefe_sec = mysql_query ("SELECT * FROM tbl_jefe_psectoriales AS j_sectorial 
                             WHERE j_sectorial .cedula = '$_REQUEST[cedula]'", $enlace) or
                                 die("Problemas en el select:".mysql_error());
	 				 
  
    $totalRows_j_sec = mysql_num_rows($jefe_sec);
		$j_sec= mysql_fetch_array($jefe_sec);
		
		// SELECT A CEDULA PARA VALIDAR CON TBL_PATRULLEROS_SECTORIAL//
//$patrulleros_sec = mysql_query ("SELECT * FROM tbl_patrulleros_sec AS patrulleros_sec
                            // WHERE patrulleros_sec.cedula = '$_REQUEST[cedula]'", $enlace) or
                                // die("Problemas en el select:".mysql_error());
	 				 
  
    //$totalRows_p_sec = mysql_num_rows($patrulleros_sec);
		//$p_sec= mysql_fetch_array($patrulleros_sec);
	
//GENERO MENSAJES



   if($totalRows_delegados>0 or $totalRows_j_circulo>0 or $totalRows_j_ubch>0 or $totalRows_j_sec>0 /*or $totalRows_p_sec*/){
	   
   if( $delegados["anillo"] or $j_circulo["anillo"] or $j_ubch["anillo"] or $j_sec["anillo"] /*or $p_sec["anillo"] >0*/){
	
	$anillo1= max(array($delegados["anillo"], $j_circulo["anillo"] ,$j_ubch["anillo"],$j_sec["anillo"]/*,$p_sec["anillo"]*/)) ;
	
	}
	
	 $anillo = $anillo1;

switch ($anillo) {
    case 1:
       $cargo="Delegado PSUV";
        break;
    case 2:
        $cargo="Jefe de Circulo";
        break;
    case 3:
        $cargo="Jefe de UBCH";
        break;
	case 4:
           $cargo="Jefe de Patrulla Sectorial ";
        break;
}
   
    $mensaje ="ERROR: Cédula ".$_REQUEST["cedula"]." <br> Ya esta registrada como ".$cargo;
   }

					 else if(isset($_REQUEST['combo2'])){
						
						
					// SELECT A  DE JEFE CIRCULO SEGUN  EL CIRCULO SELECCIONADO PARA VALIDAR QUE ESTE DIPONIBLE//
				$jefe_circulo = mysql_query ("SELECT nombre_apellido FROM tbl_jefe_circulo 
				WHERE codigo_circulo = '$_REQUEST[combo2]'", $enlace) or
					  die("Problemas en el select:".mysql_error());
					  
						$totalRows_jefe = mysql_num_rows($jefe_circulo);
						$jefe= mysql_fetch_array($jefe_circulo );
					
					 
						//GENERO MENSAJES
					
							if($totalRows_jefe>0){
						  $mensaje ="ERROR: El jefe del circulo seleccionado ya fue registrado con  <br>
						  Nombre:".$jefe["nombre_apellido"].'<br>
						 Cédula:'.$jefe["cedula"];
								 
						  }
							 						 
							 
	else {

    $reg_jefe_circulo = mysql_query("INSERT INTO tbl_jefe_circulo
	(id_parroquia,codigo_circulo,nombre_apellido,nacionalidad,cedula,sexo,telefono,correo,usuario,anillo)
     VALUES 
    ('$_REQUEST[combo1]','$_REQUEST[combo2]','$_REQUEST[nombre_apellido]',
 '$_REQUEST[nacionalidad]','$_REQUEST[cedula]','$_REQUEST[sexo]','$_REQUEST[telefono]','$_REQUEST[correo]','$_REQUEST[usuario]',
 2)", $enlace) or
  die("Problemas en el INSERT".mysql_error());
  
   
       $mensaje='¡Jefe Circulo insertado con exitó!';
   
}}}
		
			?>
            
 

<body >



<?php include "../menu/menu.php" ?>


<!---------------------- FIN MENÚ---------------------------------->
<ol class="breadcrumb">
  <li><a href="#">Inicio</a></li>
  <li class="active"> REGISTRAR JEFE DE CIRCULO</li>
</ol>

<?php if (isset($mensaje)){ ?>
         <div id="mensaje" title="RESULTADO...">
    <?php 
		echo $mensaje;} ?>
    </div>   


<form id="form1" name="form1" action="?" method="POST" >

    <input type="hidden" name="usuario" value="<?php echo $_SESSION['usuario']; ?>"> 

<div  id="select_dependiente">
 <p class="titulo_iniciarS">
                      SELECCIONE
                  </p>
<div style="padding:2%;">
<div class="form-group" >
<label> PARROQUIA <span class="obligatorio">*</span></label>
<select name="combo1" id="combo1" class="form-control" required>	
	    
   <?php 
	///////////////////////////SELECT PARA OBTENER PARROQUIAS///////////////////////////////////////////
	$parroquias=mysql_query("SELECT * FROM tbl_parroquias",$enlace);
	 echo ("<option>PARROQUIA</option>"); 
	while($row=mysql_fetch_array($parroquias)){
										
          echo ' <option value="'.$row['id_parroquia'].'">'.$row['parroquia'].'</option>';	
								
}?>		 
  
</select>
</div>


<div class="form-group">
<label> CIRCULO DE LUCHA <span class="obligatorio">*</span> </label>
<select name="combo2" id="combo2" class="form-control" required>
<option value=""> CIRCULO DE LUCHA </option>	
</select>
</div>




</div>
 <a href="#" onClick="buscarCirculo(); return false" name="verifica"  id="verifica">
  <span class="glyphicon glyphicon-search"></span></a>

<div id="resultado_circulo">   </div><!-- FIN RESULTADO CIRCULO --->
</div><!--FIN SELECT DEPENDIENTE-->



<div class="datos_complementarios"> 
<p class="titulo_iniciarS">
                    REGISTRAR JEFE DE CIRCULO
                  </p>


<div id="formulario" style="padding:0 3% 3%;">
<div class="form-group">
<label> Cédula <span class="obligatorio">*</span></label>
<div class="form-inline">




<select id="nacionalidad" name="nacionalidad" class="form-control" required>
<option value="">Nacionalidad </option>
<option value="venezolana"> V </option>
<option value="extranjera"> E </option>
</select>

<input type="text"  class="form-control" maxlength="30"  id="cedula" name="cedula"  placeholder="Cedula"
onKeyPress="return acceptNum(event)" required />
  <a href="#" onClick="buscarDato(); return false" name="verifica"  id="verifica">
  <span class="glyphicon glyphicon-search"></span></a>

</div>

</div>


 <div id="resultado">
 
 
         <div class="form-group">       	   
	 <label for="nombre">Nombre y Apellido<span class="obligatorio">*</span></label>		 
    <input type="text" class="form-control" id="nombre"  name="nombre_apellido" placeholder="Nombre y Apellido" required>
    </div>
    
    
   

<div class="form-group">
<label >Sexo </label>
<select id="sexo" name="sexo" class="form-control">
<option value="">Sexo </option>
<option value="F"> Femenino </option>
<option value="M"> Masculino</option>
</select>

</div>



<!--<input type ="text" name="nacionalidad" placeholder="NACIONALIDAD" class="input-medium">-->



<div class="form-group">
<div class=" form-horizontal">

<label> Telefono  </label>
<input type="tel" name="telefono" placeholder="Telefono" class="form-control">

<label>Correo <span class="obligatorio">*</span></label>
<input type="email" name="correo" placeholder="email@ejemplo.COM" class="form-control" >
</div>

</div>
 
     
        
<div  align="center">
<button type="reset" name="restablecer" class="btn-danger" >
Restablecer  <span class="glyphicon glyphicon-refresh" aria-hidden="true">
</span></button>

<button type="submit" name="registrar" class=" btn-success" >
Registrar   <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true">
</span></button>
</div>











</div><!--- FIN DIV FORMULARIO----->
</div><!-- FIN DATOS COMPLEMENTARIO-->


</div>




</div><!---------------------------------FIN CONTAINER-------------------------------------->

</form><!-- FIN FORMULARIO reg_jefe_circulo-->


</body>
</html>
