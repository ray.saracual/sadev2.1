<?php 
include"../conexion/sesion.php";
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>SADEV2.1 | Jefe Circulo de Lucha</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.theme.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.css">


<!-- Latest compiled and minified JavaScript -->
<script src="../js/jquery-1.11.1.min.js"></script>
        <script src="../dist/js/bootstrap.js"></script>
      <script src="../jui/jquery-ui-1.11.2.custom/jquery-ui.min.js"></script>

<script language="javascript">
$(document).ready(function(){
	// Parametros para e combo1
   $("#combo1").change(function (valor) {
	    
	   
	    $("#combo1 option:selected").each(function () {
			//alert($(this).val());
				elegido=$(this).val();
				$.post("combo1.php", { elegido: elegido }, function(data){
				$("#combo2").html(data);
				$("#combo3").html();
				
			});
			
        });
		
   })
   
  		
	// Parametros para el combo2
	$("#combo2").change(function () {
   		$("#combo2 option:selected").each(function () {
			 //alert($(this).val());
				elegido=$(this).val();
				$.post("combo2.php", { elegido: elegido }, function(data){
				$("#combo3").html(data);
			});			
        });
   })
   
    $( "#mensaje" ).dialog({
      hide:"explode",
      modal: true,
	
	  
	  
   
  });
   
   
});




</script>

<?php
include "../conexion/db.php";

// Conexión a la base de datos

$enlace  = conectar();
$mensaje = '';

   ?>

</head>



<body >

<?php 
include ("../menu/menu.php");
?>


<ol class="breadcrumb">
  <li><a href="#">Inicio</a></li>
  <li class="active">Listados</li>
</ol>


<div class="jumbotron" style="margin:2%;">
  <h1 style=" padding:2%">Listados </h1>
  
  
<p style=" padding-left:2%">
Listados por CLP
<span class="glyphicon glyphicon-list-alt" > </span>
</p>


<p style=" padding-left:2%">
<a href="select_pq_jubch.php"> Jefes de UBCH </a>
<span class="glyphicon glyphicon-list-alt" > </span>
</p>


<p style=" padding-left:2%">
  <a href="selec_list_ubch.php">Listados por UBCH </a> 
  <span class="glyphicon glyphicon-list-alt" > </span></p>
  <!--<p style=" padding:2%"><a class="btn btn-danger" href="#" role="button">Seleccione una opción del menú superior.</a></p>-->


<p style=" padding-left:2%">
 
  <a href="reg_p_t1.php" >Patrullas Sectoriales</a>
  <span class="glyphicon glyphicon-list-alt" > </span></p>
 
</div>


<div id="mensaje" title="SELECCIONE...">
<form id="form1" name="form1" action="listado_por_ubch.php" method="POST" >

   

<div  id="select_dependiente">

<div class="form-group">
<label> PARROQUIA <span class="obligatorio">*</span></label>
<select name="combo1" id="combo1" class="form-control" required>	
	    
   <?php 
   
   
   
	///////////////////////////SELECT PARA OBTENER PARROQUIAS///////////////////////////////////////////
	$parroquias=mysql_query("SELECT * FROM tbl_parroquias",$enlace);
	 echo ("<option>PARROQUIA</option>"); 
	while($row=mysql_fetch_array($parroquias)){
										
          echo ' <option value="'.$row['id_parroquia'].'">'.$row['parroquia'].'</option>';	
								
}?>		 
  
</select>
</div>

<input type="hidden" value="<?php echo $row['parroquia'] ?>" name="parroquia">
<div class="form-group">
<label> CIRCULO DE LUCHA <span class="obligatorio">*</span> </label>
<select name="combo2" id="combo2" class="form-control"  required>
<option value="">CIRCULO DE LUCHA </option>	
</select>
</div>

<div class="form-group">
<label> UBCH <span class="obligatorio">*</span> </label>
<select name="combo3" id="combo3" class=" form-control" required>	
<option value="">SELECIONE UBCH </option>	
</select>

</div>




</div><!--FIN SELECT DEPENDIENTE-->


        
<div class="bg-danger" align="center">
<button type="reset" name="restablecer" class="btn-danger" >
Restablecer  <span class="glyphicon glyphicon-refresh" aria-hidden="true">
</span></button>

<button type="submit" name="registrar" class=" btn-success" >
Procesar   <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true">
</span></button>
</div>








</form><!-- FIN FORMULARIO reg_jefe_circulo-->
</div><!-- FIN DIALOG O DIV MENSAJE -->
</body>
</html>