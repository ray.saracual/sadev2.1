<?php 
include"../conexion/sesion.php";
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
<meta charset="utf-8">
<title>SADEV2.1 | Listado por UBCH </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.theme.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.css">


<style>
#container{
	margin-left:auto;
	margin-right:auto;
	
	
	}
#select_dependiente {
	
	margin-left:3%;
	margin-bottom:3%;

	border-radius:5%;
box-shadow : rgba(0,0,0,0.3) 0px 0px 1em;

width:auto;

	}
.datos_complementarios {
	margin-left:5%;
	margin-right:auto;
	box-shadow : rgba(0,0,0,0.3) 0px 0px 1em;	
	margin-bottom:5%;
	width:90%;
	
}
	
	
	#select_dependiente ,.datos_complementarios{
		display: inline-block;
		vertical-align:top;
			
		
		}		
		
		
		
		.titulo_iniciarS{
		
		background-color:#900; 
		height:30px; 
		padding-top:5px; 
		color:#FFF;
		text-align:center;
		width:100%;
			
		
		}

</style>


<!-- Latest compiled and minified JavaScript -->
<script src="../js/jquery-1.11.1.min.js"></script>
   <script src="../js/jquery-ui.min.js"></script>
     <script src="../dist/js/bootstrap.js"></script>


   

</head>

<?php 

include "../conexion/db.php";
// Conexión a la base de datos

$enlace  = conectar();

///////////////////////////SELECT PARA OBTENER PARROQUIA ///////////////////////////////////////////
	 $parroquia=mysql_query("SELECT * FROM tbl_parroquias AS pq
	 WHERE pq.id_parroquia = $_REQUEST[combo1]",$enlace);
	$totalRows_parroquia = mysql_num_rows ($parroquia);
	$pq=mysql_fetch_array($parroquia);
	
			?>
            
 

<body >

<nav id="menu">

<?php include "../menu/menu.php" ?>

</nav>
<!---------------------- FIN MENÚ---------------------------------->


<ol class="breadcrumb">
  <li><a href="#">INICIO</a></li>
  <li >LISTADO POR UBCH</li>
  <li class="active"><?php echo $pq['parroquia'] ?></li>
  <li class="active">CIRCULO &nbsp; <?php echo $_REQUEST['combo2'] ?></li>
  <li class="active">UBCH &nbsp;<?php echo $_REQUEST['combo3'] ?></li>
</ol>






<div class="datos_complementarios"> 

<p class="titulo_iniciarS">
                    JEFE DE CIRCULO DE LUCHA POPULAR
                  </p>

	 <?php 
	 
	 if (isset($_REQUEST['combo2'])){
	 
	///////////////////////////SELECT PARA OBTENER JEFE CIRCULO ///////////////////////////////////////////
	$jefe_circulo=mysql_query("SELECT * FROM tbl_jefe_circulo AS j_circulo JOIN tbl_circulos_de_lucha AS c_lucha
	ON j_circulo.codigo_circulo = c_lucha.codigo_circulo
	 WHERE j_circulo.codigo_circulo = $_REQUEST[combo2]",$enlace);
	$totalRows_j_circulo = mysql_num_rows ($jefe_circulo);
	 
	
	if($totalRows_j_circulo>0){?>
    	<div class="table-responsive"  style="padding-left:0.5%; padding-right:0.5%;"> 
<table class="table table-bordered">
    <thead>
        <tr class="active">
            <th>CODIGO DE CIRCULO</th>
            <th>NOMBRE DEL CIRCULO</th>
            <th>JEFE DEL CIRCULO</th>
            <th>CÉDULA</th>
            <th>TELEFONO</th>
            
        </tr>
    </thead>  
    <?php
	$cont=0;
	if($row=mysql_fetch_array($jefe_circulo)){
		$cont=$cont+1;								
         
		  ?>
    <tbody>
        <tr class="brillo">
            <td><?php echo $_REQUEST['combo3']  ?></td>
             <td><?php echo $row["nombre_circulo"] ?></td>
            <td><?php echo $row["nombre_apellido"] ?></td>
            <td><?php echo $row["cedula"] ?></td>
             <td><?php echo $row["telefono"] ?></td>
        </tr>
		  <?php
								
}}else {  ?><!--- CIERRO WHILE DE JEFES DE PATRULLAS SECTORIALES Y IF CUANDO TOTAL ROW JEFE ES MAYOR A 0 "CERO"----------------->		   
            <p align="center">NO HAY REGISTROS.</p>



 <?php } ?>
    </tbody>
</table>




<p class="titulo_iniciarS">
                    JEFE DE UBCH
                  </p>

	 <?php 
	///////////////////////////SELECT PARA OBTENER JEFE UBCH ///////////////////////////////////////////
	$jefe_ubch=mysql_query("SELECT * FROM tbl_jefe_ubch AS j_ubch JOIN tbl_centros_2014_clp AS centros_2014
	ON j_ubch.codigo_centro=centros_2014.codigo_centro
	 WHERE j_ubch.codigo_centro = $_REQUEST[combo3]",$enlace);
	$totalRows_j_ubch = mysql_num_rows ($jefe_ubch);
	
	
	if($totalRows_j_ubch>0){?>
    	<div  style="padding-left:0.5%; padding-right:0.5%;"> 
<table class="table table-bordered">
    <thead>
        <tr class="active">
            <th>CODIGO DE UBCH</th>
            <th>NOMBRE DE UBCH</th>
            <th>JEFE DE UBCH</th>
            <th>CÉDULA</th>
            <th>TELEFONO</th>
            
        </tr>
    </thead>  
    <?php
	$cont=0;
	if($row=mysql_fetch_array($jefe_ubch)){
		$cont=$cont+1;								
         
		  ?>
    <tbody>
        <tr class="brillo">
            <td><?php echo $_REQUEST['combo3']  ?></td>
             <td><?php echo $row["nombre_centro"] ?></td>
            <td><?php echo $row["nombre_apellido"] ?></td>
            <td><?php echo $row["cedula"] ?></td>
             <td><?php echo $row["telefono"] ?></td>
        </tr>
		  <?php
								
}}else {  ?><!--- CIERRO WHILE DE JEFES DE PATRULLAS SECTORIALES Y IF CUANDO TOTAL ROW JEFE ES MAYOR A 0 "CERO"----------------->		   
            <p align="center">NO HAY REGISTROS.</p>



 <?php } ?>
    </tbody>
</table>




<p class="titulo_iniciarS">
                    JEFES DE PATRULLAS SECTORIALES
                  </p>

 <?php 
	///////////////////////////SELECT PARA OBTENER LOS 10 ///////////////////////////////////////////
	$jefe_p_sectorial=mysql_query("SELECT * FROM tbl_jefe_psectoriales AS jp_sectorial JOIN tbl_p_sectoriales AS p_sectorial
	ON jp_sectorial.id_p_sectorial=p_sectorial.id_p_sectorial WHERE codigo_centro = $_REQUEST[combo3]",$enlace);
	$totalRows_p_sectorial = mysql_num_rows ($jefe_p_sectorial);
	
	
	if($totalRows_p_sectorial>0){?>
    	<div   style="padding-left:0.5%; padding-right:0.5%;"> 
<table class="table table-bordered">
    <thead>
        <tr class="active">
            <th>#</th>
            <th width="50%">Patrulla Sectorial </th>
            <th>Nombre y Apellido</th>
            <th>Cédula</th>
        </tr>
    </thead>  
    <?php
	$cont=0;
	while($row=mysql_fetch_array($jefe_p_sectorial)){
		$cont=$cont+1;								
         
		  ?>
    <tbody>
        <tr class="brillo">
            <td><?php echo $cont ?></td>
             <td><?php echo $row["p_sectorial"] ?></td>
            <td><?php echo $row["nombre_apellido"]?></td>
            <td><?php echo $row["cedula"] ?></td>
        </tr>
		  <?php
								
}}else {  ?><!--- CIERRO WHILE DE JEFES DE PATRULLAS SECTORIALES Y IF CUANDO TOTAL ROW JEFE ES MAYOR A 0 "CERO"----------------->		   
            <p align="center">NO HAY REGISTROS.</p>



 <?php }} ?>
    </tbody>


</table>
 <div style="text-align:center;">
 <a class="btn btn-mini" href="reportes/listado_por_ubch2.php?combo3=<?php echo $_REQUEST['combo3']; ?>" target="new">
                <span class="glyphicon glyphicon-print"> IMPRIMIR</span>
              </a>
</div>
</div><!--- FIN TABLE RESPONSIVE-------->

</div>

 
</div><!-- FIN DATOS COMPLEMENTARIO-->

<?php
 mysql_free_result($parroquia);  
 mysql_free_result($jefe_p_sectorial);
 mysql_free_result($jefe_ubch)  ;  
  mysql_free_result($jefe_circulo)         

?>
</body>
</html>