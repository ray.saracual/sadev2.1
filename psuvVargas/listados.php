<?php 
include"../conexion/sesion.php";
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>SADEV2.1 | LISTADOS </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.css">

<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.theme.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.css">



<!-- Latest compiled and minified JavaScript -->
<script src="../js/jquery-1.11.1.min.js"></script>
   <script src="../js/jquery-ui.min.js"></script>
     <script src="../dist/js/bootstrap.js"></script>
 <script src="../jui/jquery-ui-1.11.2.custom/jquery-ui.min.js"></script>

<body>

<?php 
include "../menu/menu.php" ?>


<!---------------------- FIN MENÚ---------------------------------->
<ol class="breadcrumb">
  <li><a href="#">Inicio</a></li>
  <li class="active">LISTADOS</li>
</ol>



<div class="jumbotron" style="margin:2%;">
  <h1 style=" padding:2%">Listados </h1>
  
  
<p style=" padding-left:2%">
<a href="select_pq.php"> Listados por CLP </a>
<span class="glyphicon glyphicon-list-alt" > </span>
</p>

<p style=" padding-left:2%">
<a href="select_pq_jubch.php"> Jefes de UBCH</a>
<span class="glyphicon glyphicon-list-alt" > </span>
</p>
<p style=" padding-left:2%">
  <a href="selec_list_ubch.php">Listados por UBCH</a> 
  <span class="glyphicon glyphicon-list-alt" > </span></p>
  <!--<p style=" padding:2%"><a class="btn btn-danger" href="#" role="button">Seleccione una opción del menú superior.</a></p>-->


<p style=" padding-left:2%">
 
  <a href="buscar_p_sectorial.php" >Patrullas Sectoriales</a>
  <span class="glyphicon glyphicon-list-alt" > </span></p>
  <!--<p style=" padding:2%"><a class="btn btn-danger" href="#" role="button">Seleccione una opción del menú superior.</a></p>-->
</div>


</body>
</html>