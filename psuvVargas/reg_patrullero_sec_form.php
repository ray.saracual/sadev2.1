<?php
include"../conexion/sesion.php";
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
<meta charset="utf-8">
<title>SADEV2.1 | Patrullas Sectoriales </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.theme.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.css">

<style>
#container{
	margin-left:auto;
	margin-right:auto;
	
	
	}
#select_dependiente {
	
		
	}
.datos_complementarios {
	margin-left:auto;
	margin-right:auto;
	box-shadow : rgba(0,0,0,0.3) 0px 0px 1em;
	
	margin-bottom:5%;
	width:70%;
	
	border-radius:3%;

}
	.datos_complementarios label {
		
		}
	#select_dependiente ,.datos_complementarios{
				
		}		

		.titulo_iniciarS{
		border-top-left-radius: 10px;
        border-top-right-radius: 10px; 
		background-color:#900; 
		height:30px; 
		padding-top:5px; 
		color:#FFF;
		text-align:center;
		width:100%;
		
		}

</style>

<!-- Latest compiled and minified JavaScript -->
<script src="../js/jquery-1.11.1.min.js"></script>
   <script src="../js/jquery-ui.min.js"></script>
     <script src="../dist/js/bootstrap.js"></script>
 <script src="../jui/jquery-ui-1.11.2.custom/jquery-ui.min.js"></script>

<script language="javascript">
$(document).ready(function(){
	// Parametros para e combo1
   $("#combo1").change(function (valor) {
	    
	   
	    $("#combo1 option:selected").each(function () {
			//alert($(this).val());
				elegido=$(this).val();
				$.post("combo1.php", { elegido: elegido }, function(data){
				$("#combo2").html(data);
				$("#combo3").html();
				
			});
			
        });
		
   })
  		
	// Parametros para el combo2
	$("#combo2").change(function () {
   		$("#combo2 option:selected").each(function () {
			 //alert($(this).val());
				elegido=$(this).val();
				$.post("combo2.php", { elegido: elegido }, function(data){
				$("#combo3").html(data);
			});			
        });
   })
   
    $( "#mensaje" ).dialog({
      hide:"explode",
      modal: true,
   
  });

});


function nuevoAjax(){
	var xmlhttp=false;
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}catch(E){
			xmlhttp = false;
		}
	}

	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	
	return xmlhttp;
}

function buscarDato(){
		
	resul = document.getElementById('resultado');
	
	//bus=document.frmbusqueda.nro_acta.value;
	bus=document.form1.cedula.value;
	
	ajax=nuevoAjax();
	ajax.open("POST", "busqueda.php",true);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			resul.innerHTML = ajax.responseText
		}
	}
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send("busqueda="+bus)

}

function buscarUbchPatrulla(){
		
	resul = document.getElementById('resultado_ubchP');
	
	//bus=document.frmbusqueda.nro_acta.value;
	bus=document.form1.combo3.value;
	
	ajax=nuevoAjax();
	ajax.open("POST", "busqueda_ubch_P.php",true);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			resul.innerHTML = ajax.responseText
		}
	}
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send("busqueda="+bus)

}

var nav4 = window.Event ? true : false;
function acceptNum(evt){ 
// NOTE: Backspace = 8, Enter = 13, '0' = 48, '9' = 57 
var key = nav4 ? evt.which : evt.keyCode; 
return (key <= 13 || (key >= 48 && key <= 57));

}

</script>


</head>

<?php 

include "../conexion/db.php";

// Conexión a la base de datos

$enlace  = conectar();


///////////////////////////INSERTANDO FORMULARIO///////////////////////////////////////////

if (isset($_POST["cedula"])){
$cedula = $_REQUEST['cedula'];

		

// SELECT A CEDULA DE DELEGADO PARA VALIDAR//
//$delegados_psuv = mysql_query ("SELECT * FROM tbl_delegados AS delegados  
                            // WHERE delegados.cedula = '$_REQUEST[cedula]'", $enlace) or
                                 //die("Problemas en el select:".mysql_error());
	 				 
  
   // $totalRows_delegados = mysql_num_rows($delegados_psuv);
		//$delegados= mysql_fetch_array($delegados_psuv );
	
	// SELECT A CEDULA DE DELEGADO PARA VALIDAR CON TBL_JEFE_CIRCULO//
//$jefe_circulo = mysql_query ("SELECT * FROM tbl_jefe_circulo AS j_circulo 
                            // WHERE j_circulo.cedula = '$_REQUEST[cedula]'", $enlace) or
                                 //die("Problemas en el select:".mysql_error());
	 				 
  
   // $totalRows_j_circulo = mysql_num_rows($jefe_circulo);
	//	$j_circulo= mysql_fetch_array($jefe_circulo);
	
	// SELECT A CEDULA DE DELEGADO PARA VALIDAR CON TBL_JEFE_UBCH//
//$jefe_ubch = mysql_query ("SELECT * FROM tbl_jefe_ubch AS j_ubch 
                           //  WHERE j_ubch.cedula = '$_REQUEST[cedula]'", $enlace) or
                                // die("Problemas en el select:".mysql_error());
	 				 
  
    //$totalRows_j_ubch = mysql_num_rows($jefe_ubch);
		//$j_ubch= mysql_fetch_array($jefe_ubch );
		
		// SELECT A CEDULA DE DELEGADO PARA VALIDAR CON TBL_JEFE_PSECTORIAL//
//$jefe_sec = mysql_query ("SELECT * FROM tbl_jefe_psectoriales AS j_sectorial 
                             //WHERE j_sectorial .cedula = '$_REQUEST[cedula]'", $enlace) or
                                // die("Problemas en el select:".mysql_error());
	 				 
  
    //$totalRows_j_sec = mysql_num_rows($jefe_sec);
		//$j_sec= mysql_fetch_array($jefe_sec);
		
		// SELECT A CEDULA DE DELEGADO PARA VALIDAR CON TBL_PATRULLEROS_SECTORIAL//
$patrulleros_sec = mysql_query ("SELECT * FROM tbl_patrulleros_sec AS patrulleros_sec
                              JOIN tbl_parroquias AS pq ON patrulleros_sec.id_parroquia=pq.id_parroquia
							  WHERE patrulleros_sec.cedula = '$_REQUEST[cedula]'", $enlace) or
                                 die("Problemas en el select:".mysql_error());
	 				 
  
    $totalRows_p_sec = mysql_num_rows($patrulleros_sec);
		$p_sec= mysql_fetch_array($patrulleros_sec);
	
//GENERO MENSAJES



   if(/*$totalRows_delegados>0 or $totalRows_j_circulo>0 or $totalRows_j_ubch>0 /*or $totalRows_j_sec>0 or*/ $totalRows_p_sec >0){
	   
   /*if( $delegados["anillo"] or $j_circulo["anillo"] or $j_ubch["anillo"] or $j_sec["anillo"]  /*or $p_sec["anillo"] >0){
	
	$anillo1= max(array($delegados["anillo"], $j_circulo["anillo"] ,$j_ubch["anillo"],$j_sec["anillo"],/*$p_sec["anillo"])) ;
	
	}
	
	 $anillo = $anillo1;

switch ($anillo) {
    case 1:
       $cargo="Delegado PSUV";
        break;
    case 2:
        $cargo="Jefe de Circulo";
        break;
    case 3:
        $cargo="Jefe de UBCH";
        break;
	case 4:
           $cargo="Jefe de Patrulla Sectorial o Patrullero Sectorial";
        break;
}*/
   //GENERO MENSAJES
    $mensaje ="ERROR: Cédula ".$_REQUEST["cedula"]." <br> Ya esta registrada como Patrullero Sectorial en la ".$p_sec["parroquia"]."<br> Centro " .$p_sec["codigo_centro"];
   }



			
			  

					 else if(isset($_REQUEST['codigo_centro'])){
						
						
					// SELECT A  TBL_JEFE_PSECTORIAL  SEGUN CENTRO EL CENTRO SELECCIONADO PARA VALIDAR CUANTAS PATRULLAS SE HAN REGISTRADO//
				$psectorial = mysql_query ("SELECT * FROM tbl_patrulleros_sec AS patrullero_sec 
 JOIN tbl_p_sectoriales AS psectorial ON patrullero_sec.id_p_sectorial= psectorial.id_p_sectorial
 WHERE patrullero_sec.codigo_centro = '$_REQUEST[codigo_centro]' AND patrullero_sec.id_p_sectorial='$_REQUEST[id_p_sectorial]'", $enlace) or
					  die("Problemas en el select:".mysql_error());
					  
						$totalRows_patrullero = mysql_num_rows($psectorial);
						$patrulleros_sec= mysql_fetch_array($psectorial);
					   
					
					 
					 
						//GENERO MENSAJES
					
							if($totalRows_patrullero==12){
						  $mensaje ="ERROR: La patrulla seleccionada ya posee 12 patrulleros registrados <br>";
								 
						  }
	
	else{

    $reg_jefe_ubch = mysql_query("INSERT INTO tbl_patrulleros_sec
	(id_parroquia,codigo_circulo,codigo_centro,	id_p_sectorial,sexo,nombre_apellido,nacionalidad,
	cedula,telefono,correo,usuario,anillo)
     VALUES 
  ('$_REQUEST[id_parroquia]','$_REQUEST[codigo_circulo]','$_REQUEST[codigo_centro]','$_REQUEST[id_p_sectorial]',
  '$_REQUEST[sexo]','$_REQUEST[nombre_apellido]','$_REQUEST[nacionalidad]','$_REQUEST[cedula]','$_REQUEST[telefono]','$_REQUEST[correo]','$_REQUEST[usuario]',5)", $enlace) or
  die("Problemas en el INSERT".mysql_error());
  
   
       $mensaje='¡Patrullero Sectorial insertado con exitó!';
   
}}}



			?>

<body >

<nav id="menu">

<?php include "../menu/menu.php" ?>

</nav>
<!---------------------- FIN MENÚ---------------------------------->

<?php 


if(isset($_POST["codigo"]))
	$codigo= $_REQUEST["codigo"];
	
// SELECT DATOS A OBTENER SEGUN ID_PATRULLERO_SEC//
$jefe_p_sec = mysql_query ("SELECT * FROM tbl_jefe_psectoriales as j_psec JOIN tbl_parroquias AS pq 
                           ON  j_psec.id_parroquia = pq.id_parroquia  JOIN tbl_p_sectoriales AS p_sectoriales
						   ON j_psec.id_p_sectorial = p_sectoriales.id_p_sectorial 
						   WHERE id_jefe_psectorial = '$_REQUEST[codigo]'", $enlace) or
  die("Problemas en el select esre:".mysql_error());
  
	 $totalRows_j_sec = mysql_num_rows($jefe_p_sec );
		$jefe= mysql_fetch_array($jefe_p_sec  );



?>


<ol class="breadcrumb">
  <li><a href="#">Inicio</a></li>
  <li class="active"> REGISTRAR PATRULLERO SECTORIAL</li>
   <li class="active"><?php echo $jefe["parroquia"] ?></li>
   <li class="active"> CIRCULO <?php echo $jefe["codigo_circulo"] ?></li>
    <li class="active">UBCH <?php echo $jefe["codigo_centro"] ?></li>
    <li class="active"><?php echo strtoupper ($jefe["p_sectorial"]) ?></li>
</ol>

<?php if (isset($mensaje)){ ?>
         <div id="mensaje" title="RESULTADO...">
    <?php 
		echo $mensaje;} ?>
    </div> <!-- FIN NMENSAJE----------->  







<form id="form1" name="form1" action="?" method="POST" >
<input type="hidden" name="codigo" value=" <?php echo $jefe["id_jefe_psectorial"] ?>">  
<input type="hidden" name="id_parroquia" value=" <?php echo $jefe["id_parroquia"] ?>">  
<input type="hidden" name="codigo_circulo" value=" <?php echo $jefe["codigo_circulo"] ?>">   
<input type="hidden" name="codigo_centro" value=" <?php echo $jefe["codigo_centro"] ?>">
<input type="hidden" name="id_p_sectorial" value=" <?php echo $jefe["id_p_sectorial"] ?>">

  <input type="hidden" name="usuario" value="<?php echo $_SESSION['usuario']; ?>">   

<div class="datos_complementarios"> 

<p class="titulo_iniciarS">
                    REGISTRAR PATRULLERO SECTORIAL
                  </p>

<div id="formulario" style="padding:0 3% 3%;">
<p align="center" style="margin-bottom:4%;"><strong>JEFE DE PATRULLA SECTORIAL : </strong><?php echo $jefe["nombre_apellido"] ?></p>


<div class="form-group">
<label> Cédula del Patrullero <span class="obligatorio">*</span></label>
<div class="form-inline">

<select id="nacionalidad" name="nacionalidad" class="form-control" required>
<option value="">Nacionalidad </option>
<option value="venezolana"> V </option>
<option value="extranjera"> E </option>
</select>

<input type="text"  class="form-control" maxlength="30"  id="cedula" name="cedula"  placeholder="Cedula"
onKeyPress="return acceptNum(event)" required />
 <a href="#" onClick="buscarDato(); return false" name="verifica"  id="verifica"><span class="glyphicon glyphicon-search"></span></a>

</div><!---FIN FORM GROUP--->

</div><!--- fin form inline----->



 
 <div id="resultado">
 
 
 
 
         <div class="form-group">       	   
	 <label for="nombre">Nombre y Apellido<span class="obligatorio">*</span></label>		 
    <input type="text" class="form-control" id="nombre"  name="nombre_apellido" placeholder="Nombre y Apellido" required>
    </div>
    
 
 
      <!--   <div class="form-group">       	   
	 <label for="nombre">Nombre <span class="obligatorio">*</span></label>		 
    <input type="text" class="form-control" id="nombre"  name="nombre" placeholder="Nombre" required>
    </div>
    
    
    
    <div class="form-group">
    <label for="apellido">Apellido <span class="obligatorio">*</span></label>
    <input type="text" class="form-control" id="apellido"  name="apellido" placeholder="Apellido " required>
  </div>-->
        
   

<div class="form-group">
<label >Sexo </label>
<select id="sexo" name="sexo" class="form-control">
<option value="">Sexo </option>
<option value="F"> Femenino </option>
<option value="M"> Masculino</option>
</select>

</div>



<!--<input type ="text" name="nacionalidad" placeholder="NACIONALIDAD" class="input-medium">-->



<div class="form-group">
<div class=" form-horizontal">

<label> Telefono  </label>
<input type="tel" name="telefono" placeholder="Telefono" class="form-control">

<label>Correo <span class="obligatorio">*</span></label>
<input type="email" name="correo" placeholder="email@ejemplo.COM" class="form-control" >
</div>

</div>
 
     
        
<div  align="center">
<button type="reset" name="restablecer" class="btn-danger" >
Restablecer  <span class="glyphicon glyphicon-refresh" aria-hidden="true">
</span></button>

<button type="submit" name="registrar" class=" btn-success" >
Registrar   <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true">
</span></button>
</div>


</div><!--- FIN DIV FORMULARIO----->
</div><!-- FIN DATOS COMPLEMENTARIO-->

</div>

</form><!-- FIN FORMULARIO reg_jefe_circulo-->

</body>
</html>