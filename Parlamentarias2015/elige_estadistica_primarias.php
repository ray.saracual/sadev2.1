<?php 
include"../conexion/sesion.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>SADE 2.1 | Parlamentarias 2015</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.theme.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.css">
<!-- Latest compiled and minified JavaScript -->
<script src="../js/jquery-1.11.1.min.js"></script>
   <script src="../js/jquery-ui.min.js"></script>
     <script src="../dist/js/bootstrap.js"></script>
 <script src="../jui/jquery-ui-1.11.2.custom/jquery-ui.min.js"></script>

<script language="javascript">
$(document).ready(function(){
	// Parametros para e combo1
   $("#combo1").change(function (valor) {
	    
	   
	    $("#combo1 option:selected").each(function () {
			//alert($(this).val());
				elegido=$(this).val();
				$.post("combo1.php", { elegido: elegido }, function(data){
				$("#combo2").html(data);
				$("#combo3").html();
				
			});
			
        });
		
   })
   
  		
	// Parametros para el combo2
	$("#combo2").change(function () {
   		$("#combo2 option:selected").each(function () {
			 //alert($(this).val());
				elegido=$(this).val();
				$.post("combo2.php", { elegido: elegido }, function(data){
				$("#combo3").html(data);
			});			
        });
   })
   
    $( "#mensaje" ).dialog({
      hide:"explode",
      modal: true,
	
	  
	  
   
  });
   
   
});




</script>




<style>
#container{
	margin-left:auto;
	margin-right:auto;
	
	
	}
#select_dependiente {
	
	margin-left:3%;
	margin-bottom:3%;

	border-radius:5%;
box-shadow : rgba(0,0,0,0.3) 0px 0px 1em;

width:auto;
	
	

	}
.datos_complementarios {
	margin-left:3%;
	margin-right:auto;
	box-shadow : rgba(0,0,0,0.3) 0px 0px 1em;
	
	margin-bottom:5%;
	width:70%;
	
	border-radius:3%;

}
	.datos_complementarios label {
		
		
		
		}
	#select_dependiente ,.datos_complementarios{
		display: inline-block;
		vertical-align:top;
		
					
		
		}		
		
		
		
		.titulo_iniciarS{
		border-top-left-radius: 10px;
        border-top-right-radius: 10px; 
		background-color:#900; 
		height:30px; 
		padding-top:5px; 
		color:#FFF;
		text-align:center;
		width:100%;
			
		
		}

</style>
  <?php
include "../conexion/db.php";

// Conexión a la base de datos

$enlace  = conectar();

   ?>
</head>

<body>

<?php 

include ("../menu/menu_parlamentarias.php");

?>





<div class="jumbotron" style="margin:2%;">
  <h2 style=" padding:2%">Seleccione que resultados requiere visualizar </h2>
  <?php if($_SESSION['consulta']==1){ ?>
  <p style=" padding:2%"><a class="btn btn-danger" href="elige_estadistica_primarias.php?opcion1=<?php echo 1 ?>" role="button"> Resultados General</a>
  <?php }?>
  <a class="btn btn-danger" href="elige_estadistica_primarias.php?opcion2=<?php echo 2  ?>"  role="button"> Resultados por CLP</a>
  
      
</div>






<?php 
if (isset($_GET["opcion1"])&& $_SESSION['consulta']==1){ ?>
<div id="mensaje" title="SELECCIONE...">
<form id="form1" name="form1" action="estadistica_G_primarias.php" method="POST"
target="popup"
onsubmit="return confirm(' ¿Seguro que selecciono la opción correcta? \n'+ this.action)">

<!-- onsubmit="return confirm('Desea enviar formulario a:\n' + this.action)"> -->

   


<div class="form-group" >
<label> PARROQUIA <span class="obligatorio">*</span></label>
<select name="combo1" id="combo1" class="form-control" required>	
	    
   <?php 
	///////////////////////////SELECT PARA OBTENER PARROQUIAS///////////////////////////////////////////
	$parroquias=mysql_query("SELECT * FROM tbl_parroquias",$enlace);
	 echo '<option value="">PARROQUIA </option>'; 
	while($row=mysql_fetch_array($parroquias)){
										
          echo ' <option value="'.$row['id_parroquia'].'">'.$row['parroquia'].'</option>';	
								
}?>		 
  <option value="12"> ESTADAL</option>
</select>
</div>

        
<div class="bg-danger" align="center">
<button type="reset" name="restablecer" class="btn-danger" >
Restablecer  <span class="glyphicon glyphicon-refresh" aria-hidden="true">
</span></button>

<button type="submit" name="registrar" class=" btn-success" >
Procesar   <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true">
</span></button>
</div>


</form><!-- FIN FORMULARIO reg_jefe_circulo-->


</div><!-- FIN DIALOG O DIV MENSAJE -->
<?php }else ?>


<?php 
if (isset($_GET["opcion2"])){ ?>
<div id="mensaje" style="display:none" title="SELECCIONE">
 
 
    <div style="padding:2%;">
    
  <form id="form1" name="form1" action="estadistica_clp_primarias.php" method="POST" 
target="popup"
onsubmit="return confirm(' ¿Seguro que selecciono la opción correcta? \n'+ this.action)">
  
    
        <div class="form-group" >
                <label> PARROQUIA <span class="obligatorio">*</span></label>
                <select name="combo1" id="combo1" class="form-control" required>	
                        
                   <?php 
       //////////////////////////SELECT PARA OBTENER PARROQUIAS///////////////////////////////////////////
                    $parroquias=mysql_query("SELECT * FROM tbl_parroquias",$enlace);
                     echo ("<option>PARROQUIA </option>"); 
                    while($row=mysql_fetch_array($parroquias)){
                                                        
                          echo ' <option value="'.$row['id_parroquia'].'">'.$row['parroquia'].'</option>';	
                                                
                }?>		 
                  
                </select>

        </div>
  
        
<div class="form-group">
<label> CIRCULO DE LUCHA <span class="obligatorio">*</span> </label>
<select name="combo2" id="combo2" class="form-control" required>
<option value=""> CIRCULO DE LUCHA </option>	
</select>
</div>


<div class="bg-danger" align="center">
<button type="reset" name="restablecer" class="btn-danger" >
Restablecer  <span class="glyphicon glyphicon-refresh" aria-hidden="true">
</span></button>

<button type="submit" name="registrar" class=" btn-success" >
Procesar   <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true">
</span></button>
</div>
</form>
        </div>
        </div>
<?php } else?>



<?php 
if (isset($_GET["opcion3"])){ ?>

<div id="mensaje" title="SELECCIONE...">

<form id="form1" name="form1" action="estadistica_pq.php" method="POST" 
target="popup"
onsubmit="return confirm(' ¿Seguro que selecciono la opción correcta? \n'+ this.action)">


<div class="form-group">
<label> PARROQUIA <span class="obligatorio">*</span></label>
<select name="combo1" id="combo1" class="form-control" required>	
	    
   <?php 
   
   
 
	///////////////////////////SELECT PARA OBTENER PARROQUIAS///////////////////////////////////////////
	$parroquias=mysql_query("SELECT * FROM tbl_parroquias",$enlace);
	 echo ("<option>PARROQUIA </option>"); 
	while($row=mysql_fetch_array($parroquias)){
										
          echo ' <option value="'.$row['id_parroquia'].'">'.$row['parroquia'].'</option>';	
								
}?>		 
  
</select>
</div>


<div class="form-group">
<label> CIRCULO DE LUCHA <span class="obligatorio">*</span> </label>
<select name="combo2" id="combo2" class="form-control"  required>
<option value="">CIRCULO DE LUCHA </option>	
</select>
</div>

<div class="form-group">
<label> UBCH <span class="obligatorio">*</span> </label>
<select name="combo3" id="combo3" class=" form-control" required>	
<option value="">UBCH </option>	
</select>

</div>


        
<div class="bg-danger" align="center">
<button type="reset" name="restablecer" class="btn-danger" >
Restablecer  <span class="glyphicon glyphicon-refresh" aria-hidden="true">
</span></button>

<button type="submit" name="registrar" class=" btn-success" >
Procesar   <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true">
</span></button>
</div>

</form>
</div>
<?php }?>


</body>
</html>
