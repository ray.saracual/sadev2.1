<?php 
include"../conexion/sesion.php";
?>

<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
<meta charset="utf-8">
<title>SADEV2.1 | Patrullas Sectoriales </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.theme.css">
<link rel="stylesheet" type="text/css" href="../jui/jquery-ui-1.11.2.custom/jquery-ui.css">

<style>
#container{
	margin-left:auto;
	margin-right:auto;
	
	
	}
#select_dependiente {
	
	margin-left:3%;
	margin-bottom:3%;

	border-radius:5%;
box-shadow : rgba(0,0,0,0.3) 0px 0px 1em;

width:auto;

	}
.datos_complementarios {
	margin-left:5%;
	margin-right:auto;
	box-shadow : rgba(0,0,0,0.3) 0px 0px 1em;
	
	margin-bottom:5%;
	width:90%;
	
	border-radius:3%;

}
	.datos_complementarios label {
		
		
		
		}
	#select_dependiente ,.datos_complementarios{
		display: inline-block;
		vertical-align:top;
		
		}		
		.titulo_iniciarS{
		border-top-left-radius: 10px;
        border-top-right-radius: 10px; 
		background-color:#900; 
		height:30px; 
		padding-top:5px; 
		color:#FFF;
		text-align:center;
		width:100%;
			
		
		}

</style>


<!-- Latest compiled and minified JavaScript -->
<script src="../js/jquery-1.11.1.min.js"></script>
   <script src="../js/jquery-ui.min.js"></script>
     <script src="../dist/js/bootstrap.js"></script>
 <script src="../jui/jquery-ui-1.11.2.custom/jquery-ui.min.js"></script>

<script language="javascript">
$(document).ready(function(){
	// Parametros para e combo1
   $("#combo1").change(function (valor) {
	    
	   
	    $("#combo1 option:selected").each(function () {
			//alert($(this).val());
				elegido=$(this).val();
				$.post("combo1.php", { elegido: elegido }, function(data){
				$("#combo2").html(data);
				$("#combo3").html();
				
			});
			
        });
		
   })
   
  		
	// Parametros para el combo2
	$("#combo2").change(function () {
   		$("#combo2 option:selected").each(function () {
			 //alert($(this).val());
				elegido=$(this).val();
				$.post("combo2.php", { elegido: elegido }, function(data){
				$("#combo3").html(data);
			});			
        });
   })
   
    $( "#mensaje" ).dialog({
      hide:"explode",
      modal: true,
	
	  
	  
   
  });
   
   
});



function nuevoAjax(){
	var xmlhttp=false;
	try{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}catch(E){
			xmlhttp = false;
		}
	}

	if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	
	return xmlhttp;
}


function buscarDato(){
		
	resul = document.getElementById('resultado');
	
	//bus=document.frmbusqueda.nro_acta.value;
	bus=document.form1.cedula.value;
	
	ajax=nuevoAjax();
	ajax.open("POST", "busqueda.php",true);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			resul.innerHTML = ajax.responseText
		}
	}
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send("busqueda="+bus)

}

function buscarUbchPatrulla(){
		
	resul = document.getElementById('resultado_ubchP');
	
	//bus=document.frmbusqueda.nro_acta.value;
	bus=document.form1.combo3.value;
	
	ajax=nuevoAjax();
	ajax.open("POST", "busqueda_ubch_P.php",true);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			resul.innerHTML = ajax.responseText
		}
	}
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send("busqueda="+bus)

}

var nav4 = window.Event ? true : false;
function acceptNum(evt){ 
// NOTE: Backspace = 8, Enter = 13, '0' = 48, '9' = 57 
var key = nav4 ? evt.which : evt.keyCode; 
return (key <= 13 || (key >= 48 && key <= 57));

}

</script>


</head>

<?php 
include "../conexion/db.php";

// Conexión a la base de datos

$enlace  = conectar();



///////////////////////////INSERTANDO FORMULARIO///////////////////////////////////////////

if (isset($_POST["combo3"])){
$codigo_centro = $_REQUEST['combo3'];

// SELECT A CEDULA DE TBL_JEFE_PSECTORIAL PARA VALIDAR//
$jefe_Psectorial = mysql_query ("SELECT * FROM tbl_jefe_psectoriales AS jefe_psectorial JOIN tbl_parroquias
AS pq ON jefe_psectorial.id_parroquia=pq.id_parroquia  JOIN tbl_p_sectoriales AS psectorial ON psectorial.id_p_sectorial =jefe_psectorial.id_p_sectorial WHERE codigo_centro = '$_REQUEST[combo3]'", $enlace) or
  die("Problemas en el select:".mysql_error());
  
    $totalRows_jefe = mysql_num_rows($jefe_Psectorial);
		$jefe= mysql_fetch_array($jefe_Psectorial );
		$pq= $jefe["parroquia"];

}



//OBTENER DATOS PARA BREADCRUMB----------------------/

$datos_breadcrumb= mysql_query(" SELECT * FROM  tbl_centros_2014_clp AS centros_2014 
JOIN tbl_parroquias AS pq ON centros_2014.id_parroquia= pq.id_parroquia WHERE centros_2014.codigo_centro = '$_REQUEST[combo3]' ",$enlace) or
  die("Problemas en el select datos para breadcrumb:".mysql_error());
  
  $breadcrumb= mysql_fetch_array($datos_breadcrumb);
		
			?>

<body >

<nav id="menu">

<?php include "../menu/menu.php" ?>

</nav>
<!---------------------- FIN MENÚ---------------------------------->


<ol class="breadcrumb">
  <li><a href="#">Inicio</a></li>
  <li class="active">REGISTRO DE PATRULLEROS SECTORIALES</li>
  <li class="active"><?php 	echo  $breadcrumb['parroquia'] ?></li>
   <li class="active"> CIRCULO <?php echo  $_REQUEST['combo2'] ?></li>
    <li class="active">UBCH <?php echo $breadcrumb['nombre_centro'] ?></li>
</ol>

<?php if (isset($mensaje)){ ?>
         <div id="mensaje" title="RESULTADO...">
    <?php 
		echo $mensaje;} ?>
    </div> <!-- FIN NMENSAJE----------->  

<div class="datos_complementarios"> 
<p class="titulo_iniciarS">
                   REGISTRO DE PATRULLEROS SECTORIALES
                  </p>
                         
                  
               <?php 
	///////////////////////////SELECT PARA OBTENER LOS 10 ///////////////////////////////////////////
	$jefe_p_sectorial=mysql_query("SELECT * FROM tbl_jefe_psectoriales AS jp_sectorial JOIN tbl_p_sectoriales AS p_sectorial
	ON jp_sectorial.id_p_sectorial=p_sectorial.id_p_sectorial WHERE codigo_centro = $_REQUEST[combo3]",$enlace);
	$totalRows_p_sectorial = mysql_num_rows ($jefe_p_sectorial);
	
	
	if($totalRows_p_sectorial>0){?>
		
         <div class="table-responsive" style="padding:1%"> 
         <p align="center">Seleccione un Jefe de patrulla sectorial para registrar sus patrulleros. </p>
<table class="table table-bordered">
    <thead>
        <tr class="active">
            <th>#</th>
            <th>Nombre y Apellido</th>
            <th>Cédula</th>
             <th>Télefono</th>
            <th>Patrulla Sectorial </th>
            <th width="13%"> Registrar Patrulleros</th>
        </tr>
    </thead>  
        
	<?php	
	$cont=0;
	while($row=mysql_fetch_array($jefe_p_sectorial)){
			$cont=$cont+1;							
         
		  ?>
		
    <tbody>
        <tr class="brillo">
            <td><?php echo $cont; ?></td>
            <td><?php echo $row["nombre_apellido"] ?></td>
            <td><?php echo $row["cedula"] ?></td>
            <td><?php echo $row["telefono"] ?></td>
             <td><?php echo $row["p_sectorial"] ?></td>
            <td> <a class="btn btn-mini" href="reg_patrullero_sec_form.php?codigo=<?php echo $row['id_jefe_psectorial']; ?>">
            <span class="glyphicon glyphicon-plus"> </span></a>
            
                     
            
             </td>
        </tr>
   

		  
		  <?php
								
}}else {  ?><!--- CIERRO WHILE DE JEFES DE PATRULLAS SECTORIALES Y IF CUANDO TOTAL ROW JEFE ES MAYOR A 0 "CERO"----------------->		   
            <p align="center">NO HAY REGISTROS DE JEFES SECTORIALES PARA ESTA UBCH.</p>



 <?php } ?>
    </tbody>
</table>
</div><!--- FIN TABLE RESPONSIVE-------->

</div>
</div><!-- FIN DATOS COMPLEMENTARIO-->

</div>

</body>
</html>