<?php 
include"../conexion/sesion.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>SADEV 2.1|Estadistica Postulaciones </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet"  href="../dist/css/bootstrap.css">
<link rel="stylesheet"  href="../jui/jquery-ui-1.11.2.custom/jquery-ui.theme.css">
<link rel="stylesheet"  href="../jui/jquery-ui-1.11.2.custom/jquery-ui.css">
 <link rel="stylesheet" type="text/css" href="../dist/css/imprimir-estadistica.css" media="print"/>
<style>
	
		.datos_complementarios {
	margin-left:auto;
	margin-right:auto;
	box-shadow : rgba(0,0,0,0.3) 0px 0px 1em;	
	margin-bottom:2%;


}
.membrete{
	display:none;
	
	}
	.titulos_centrados {
		display:none;
		
		}
		
		.hora_reporte {
			
								
								
								}
		

</style>
<!-- Latest compiled and minified JavaScript -->
<script src="../js/jquery-1.11.1.min.js"></script>
   <!--<script src="js/jquery-ui.min.js"></script>-->
     <script src="../dist/js/bootstrap.js"></script>
 <script src="../jui/jquery-ui-1.11.2.custom/jquery-ui.min.js"></script>
<?php 
include "../conexion/db.php";

// Conexión a la base de datos

$enlace  = conectar();


///////////////////////////SELECT PARA OBTENER PARROQUIA ///////////////////////////////////////////
	
	 $parroquia=mysql_query("SELECT * FROM tbl_parroquias AS pq 
	 
	 WHERE id_parroquia =$_REQUEST[combo1]",$enlace);
	$pq=mysql_fetch_array($parroquia);
$_SESSION['todas'] = "ESTADO VARGAS";	
$_SESSION['p'] = $pq["parroquia"];
   

		
		   
	
///////////////////////////SELECT PARA OBTENER POSTULADOS PARA GRAFICA///////////////////////////////////////////	   
		   
		   
	if($_REQUEST["combo1"]==12){	 
		 
		 $postulados=mysql_query("SELECT COUNT(*) AS total ,postulados.nombre_apellido, 
postulados.cedula,postulados.telefono,SUM(votos) as total2 FROM tbl_candidatos_postulados AS postulados 
GROUP BY postulados.cedula
ORDER BY total DESC
LIMIT 0 , 20




 ",$enlace);

$totalRows_postulados = mysql_num_rows ($postulados);
		 
	} else if ( $_REQUEST["combo1"]<=11){
		
		 $postulados=mysql_query("SELECT COUNT(*) AS total ,postulados.nombre_apellido, 
postulados.cedula,postulados.telefono,SUM(votos) as total2 FROM tbl_candidatos_postulados AS postulados 
WHERE id_parroquia =  $_REQUEST[combo1]
GROUP BY postulados.cedula
ORDER BY total DESC 
LIMIT 0 , 20




 ",$enlace);

$totalRows_postulados = mysql_num_rows ($postulados);
//$row_aspirantes = mysql_fetch_array($postulados);				
		}
	
	//Recorro todos los elementos
  $k=0;

    do{
		switch ($k) {
		case 1:
			$aspirante1 = $row_aspirantes['nombre_apellido'];
	
			$voto1      = $row_aspirantes["total2"];
			break;
		case 2:
			$aspirante2 = $row_aspirantes['nombre_apellido'];
		    $voto2      = $row_aspirantes["total2"];			
			break;			
		case 3:
			$aspirante3 =$row_aspirantes['nombre_apellido'];
			 $voto3 = $row_aspirantes["total2"];					
			break;
		case 4:
			$aspirante4 = $row_aspirantes['nombre_apellido'];
			$voto4 =$row_aspirantes["total2"];				
			break;			
		case 5:
			$aspirante5 = $row_aspirantes['nombre_apellido'];
			$voto5 = $row_aspirantes["total2"];			
			break;
		case 6:
			$aspirante6 =$row_aspirantes['nombre_apellido'];
			$voto6 = $row_aspirantes["total2"];		
			break;			
		case 7:
			$aspirante7 =$row_aspirantes['nombre_apellido'];
			//$voto7 = $row_LST[total];			
			break;
		case 8:
			$aspirante8=$row_aspirantes['nombre_apellido'];
			//$voto8 = $row_LST[total];			
				
			break;
			
		
	}  
	
	$k++;
			
	}  while ($row_aspirantes = mysql_fetch_assoc($postulados));

if (isset($totalRows_postulados)>0){
?>


<script type="text/javascript" src="../js/jsapi.js"></script>
   
   
    <script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
  
   
    function drawChart() {
		  
      var data = google.visualization.arrayToDataTable([
	  
        ["Aspirantes", "Votos", { role: "style" } ],
    
	    ['<?php echo $aspirante1;?> ', <?php echo $voto1;?>, "#900"],
		   ['<?php echo $aspirante2;?> ', <?php echo $voto2;?>, "blue"],
		     ['<?php echo $aspirante3;?> ', <?php echo $voto3;?>, "yellow"],
			    ['<?php echo $aspirante4;?> ', <?php echo $voto4;?>, "green"],
				 
		
		
        
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

	
	  var options = {
        title: "REPRESENTACIÓN GRAFICA DE VOTOS OBTENIDOS",
        width: 1000,
        height: 225,
        bar: {groupWidth: "90%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }

  </script>

<?php } ?> 
     
</head>

<body>

<section>

<nav id="menu">

<?php include "../menu/menu_parlamentarias.php" ?>

</nav>



<header>
 <ol class="breadcrumb">
  <li><a href="#">Inicio</a></li>
  <li class="active">ESTADISTICA DE POSTULACIÓN PARA ELECCIONES PARLAMENTARIAS / 
  <?php if( $_REQUEST["combo1"]==12){
	  echo $_SESSION['todas']; 
	  } else {
		   echo $_SESSION['p']; 
		 
		  
	  }?> </li>
</ol>
 </header>
 
<article class="datos_complementarios">

<header class="membrete">


<img src="../images/Psuv-logo.png"  alt=""/>
<p>ESTADISTICA DE POSTULACIÓN PARA  ELECCIONES PARLAMENTARIAS <br>
<?php if( $_REQUEST["combo1"]==12){
	 echo $_SESSION['todas']; 
	  } else {
		   echo $_SESSION['p']; 
		  		  
	  }?> </li>
</p>
 <img src="../images/sala.png"  alt=""/>


</header>
 
<p class="titulo_iniciarS"> ESTADISTICA DE POSTULACIÓN PARA  ELECCIONES PARLAMENTARIAS <br>
<?php if( $_REQUEST["combo1"]==12){
	  echo $_SESSION['todas']; 
	  } else {
		   echo $_SESSION['p']; 
		  
		  
	  }?> </p>
    
           <?php  
	
	
	
///////////////////////////SELECT PARA OBTENER POSTULADOS PARA REFERENCIA///////////////////////////////////////////	   
	if($_REQUEST["combo1"]==12){	 
		 
		 $datos_centros_reg=mysql_query("SELECT COUNT(postulados.cod_centro) AS total_registrados
		  FROM tbl_candidatos_postulados AS postulados 
GROUP BY postulados.cod_centro



 ",$enlace);
///PARA CALCULAS DATOS GENERALES ESTADO VARGAS 
$totalRows_centros_reg = mysql_num_rows ($datos_centros_reg);
$row_centros_reg = mysql_fetch_array($datos_centros_reg);	
		 
	} else if ( $_REQUEST["combo1"]<=11){
		
		
		 $datos_centros_pq=mysql_query("SELECT COUNT(centros_2014.codigo_centro) AS total_ubch
FROM tbl_centros_2014_clp AS centros_2014 
WHERE id_parroquia =  $_REQUEST[combo1]
GROUP BY centros_2014.codigo_centro

 ",$enlace);

$totalRows_centros_pq= mysql_num_rows ($datos_centros_pq);
$row_centros_pq= mysql_fetch_array($datos_centros_pq);	

///PARA CALCULAR DATOS  DE VOTOS GENERALES POR CENTROS ELECTORALES
$datos_centros_reg=mysql_query("SELECT COUNT(postulados.cod_centro) AS total_registrados
  FROM tbl_candidatos_postulados AS postulados 
WHERE id_parroquia =  $_REQUEST[combo1]
GROUP BY postulados .cod_centro

 ",$enlace);

$totalRows_centros_reg_pq= mysql_num_rows ($datos_centros_reg);
$row_centros_reg = mysql_fetch_array($datos_centros_reg);	

		}
	
?>

<aside style="display:block; width:auto; vertical-align:top;">
<table class="table table-bordered">
	    <thead>
	        <tr>
	          <th>TOTAL UBCH <?php if($_REQUEST["combo1"]==12){
	 echo $_SESSION['todas']; 
	  } else {
		   echo $_SESSION['p']; 
		  }	     ?>
          
          </th>
                <th>TOTAL UBCH REGISTRADAS</th>
                 <th>PORCENTAJE DE UBCH REGISTRADAS  </th>
                 <th>TOTAL UBCH FALTANTES </th>
             
	        </tr>
	    </thead>
    
        <tbody >
	        <tr>
	            <td><?php if( $_REQUEST["combo1"]==12){
	  echo "198"; 
	  } else {
		   echo $totalRows_centros_pq; 
		  
		  
	  }?></td>
                <td><?php
				if (isset($totalRows_centros_reg )){
				   
				   echo $totalRows_centros_reg ;
					  } else{
	              echo $totalRows_centros_reg_pq;
				  
					}
				  ?></td>
                  
                  <td>
                  <?php
				if (isset($totalRows_centros_reg )){
					$porcentaje= (100*$totalRows_centros_reg)/198;
				   
				   echo number_format($porcentaje, 1, ',', ' ');
					  } else{
						  
					$porcentaje= (100*$totalRows_centros_reg_pq)/$totalRows_centros_pq;
				 
	              echo number_format($porcentaje, 1, ',', ' ');
				  
				  }?>%
					
                  </td>
	            
                <td style=" font-weight:bold"><?php 
				if (isset($totalRows_centros_reg )){
				   $faltantes= 198-$totalRows_centros_reg;
				   echo $faltantes ;
					  } else{
						  
						  $faltantes2=$totalRows_centros_pq- $totalRows_centros_reg_pq ; 
	              echo $faltantes2;
				  
					}
				  ?></td>
                  
                  
                 	          </tr>
	       
	    </tbody>
        
        
	</table>


</aside>
<?php
	
	
///////////////////////////SELECT PARA OBTENER POSTULADOS PARA TABLA ///////////////////////////////////////////	   
		
	if($_REQUEST["combo1"]==12){	 
		 
		 $postulados=mysql_query("SELECT COUNT(*) AS total ,postulados.nombre_apellido,postulados.descripcion, 
postulados.cedula,postulados.telefono,SUM(votos) as total2 FROM tbl_candidatos_postulados AS postulados 
GROUP BY postulados.cedula
ORDER BY total DESC
LIMIT 0 , 20




 ",$enlace);

$totalRows_postulados = mysql_num_rows ($postulados);
		 
	} else if ( $_REQUEST["combo1"]<=11){
		
		 $postulados=mysql_query("SELECT COUNT(*) AS total ,postulados.nombre_apellido,postulados.descripcion, 
postulados.cedula,postulados.telefono,SUM(votos) as total2 FROM tbl_candidatos_postulados AS postulados 
WHERE id_parroquia =  $_REQUEST[combo1]
GROUP BY postulados.cedula 
ORDER BY total DESC 
LIMIT 0 , 20




 ",$enlace);

$totalRows_postulados = mysql_num_rows ($postulados);


				
		}
		
	
	
		 
	  if ($totalRows_postulados>0){
	    $cont=0;?>
        
        
        <p class="titulo_iniciarS"> RELACIÓN POSTULACIONES OBTENIDAS <br></p>
<p class="titulos_centrados">  RELACIÓN POSTULACIONES OBTENIDAS </p>
	   
       <div class="table-responsive" style="font-size:12px"> 
	<table class="table table-bordered">
	    <thead style="font-size:14px">
	        <tr>
	            <th>#</th>
                <th> Descripción </th>
	            <th>Nombre y Apellido</th>
	            <th>Cédula</th>
	            <th>Teléfono</th>
                <th>Postulaciones Recibidas</th>
                <th> % Postulaciones</th>
                </tr>
	    </thead>
       <?php
	 while ($postulados_d = mysql_fetch_assoc($postulados)){
		   
	   $cont++;
	   
	   ?>
        <tbody>
	        <tr>
	            <td><?php echo $cont ?></td>
                <td><?php echo $postulados_d["descripcion"] ?></td>
	            <td><?php echo $postulados_d["nombre_apellido"] ?></td>
	            <td><?php echo $postulados_d["cedula"] ?></td>
	            <td ><?php echo $postulados_d["telefono"] ?></td>
                 <td class="negrita"><?php echo $postulados_d["total"] ?></td>
                
         <?php 
	
					if($_REQUEST["combo1"]==12){
						$divisor= 198;
						
						}
					
						else if ( $_REQUEST["combo1"]<=11){
							
							 $datos_centros_pq=mysql_query("SELECT COUNT(centros_2014.codigo_centro) AS total_ubch
					FROM tbl_centros_2014_clp AS centros_2014 
					WHERE id_parroquia =  $_REQUEST[combo1]
					GROUP BY centros_2014.codigo_centro
					
					 ",$enlace);
					
					$divisor= mysql_num_rows ($datos_centros_pq);
					$row_centros_pq= mysql_fetch_array($datos_centros_pq);
						}
						   ?>          
									 
                 
                 <td><?php 
				
				 
				 
				 $porcentaje= (100*$postulados_d["total"])/$divisor;
				 echo number_format($porcentaje, 1, ',', ' ');
				 ?></td>
                
	        </tr>
	        </tr>
	       
	    </tbody>
        
        <?php 
		 }
		   ?>
	</table>
</div>

<?php

///////////////////////////SELECT PARA OBTENER POSTULADOS PARA TABLA VOTOS  EDO VARGAS///////////////////////////////////////////	   
		
	if($_REQUEST["combo1"]==12){	 
		 
		 $postulados_votos=mysql_query("SELECT SUM(votos) as total2 ,postulados.nombre_apellido,postulados.descripcion, 
postulados.cedula,postulados.telefono FROM tbl_candidatos_postulados AS postulados 
GROUP BY postulados.cedula
ORDER BY total2 DESC

LIMIT 0 , 20




 ",$enlace);
 
 
$totalRows_postulados_votos = mysql_num_rows ($postulados_votos);
	$suma_votos=mysql_query("SELECT SUM(votos) as total_votos FROM tbl_candidatos_postulados AS postulados 

 ",$enlace);

$total_votos = mysql_fetch_array($suma_votos);


 		 
	} else if ( $_REQUEST["combo1"]<=11){
		///////////////////////////SELECT PARA OBTENER POSTULADOS PARA TABLA VOTOS  
		$postulados_votos=mysql_query("SELECT SUM(votos) as total2 ,postulados.nombre_apellido,postulados.descripcion, 
postulados.cedula,postulados.telefono FROM tbl_candidatos_postulados AS postulados  WHERE postulados.id_parroquia = $_REQUEST[combo1]
GROUP BY postulados.cedula
ORDER BY total2 DESC

LIMIT 0 , 20




 ",$enlace);

$totalRows_postulados_votos = mysql_num_rows ( $postulados_votos);



		$suma_votos=mysql_query("SELECT SUM(votos) as total_votos FROM tbl_candidatos_postulados AS postulados  WHERE postulados.id_parroquia = $_REQUEST[combo1]
		ORDER BY total_votos DESC


 ",$enlace);

$total_votos = mysql_fetch_array($suma_votos);

		}

 
	  if ($totalRows_postulados_votos>0){
	    $cont=0;?>
	   
       
       <p class="titulo_iniciarS"> RELACIÓN VOTOS OBTENIDO </p>
       
        <p class="titulos_centrados"> RELACIÓN VOTOS OBTENIDOS </p>
       <div class="table-responsive" style="font-size:12px"> 
	<table class="table table-bordered">
	    <thead style="font-size:14px">
	        <tr>
	            <th>#</th>
                <th>Descripción </th>
	            <th>Nombre y Apellido</th>
	            <th>Cédula</th>
	            <th>Teléfono</th>
                <th >Votos</th>
                <th >%</th>
                
                </th> 
                
                 <!--<th>Valor Porcentual Votos  <?php //echo $total_sum; ?></th>-->
                 
	        </tr>
	    </thead>
        
        
         <?php
	 while ($postulados_v = mysql_fetch_assoc($postulados_votos)){
		   
	   $cont++;
	   
	   ?>
        
         <tbody>
	        <tr>
	            <td><?php echo $cont ?></td>
                <td><?php echo $postulados_v["descripcion"] ?></td>
	            <td><?php echo $postulados_v["nombre_apellido"] ?></td>
	            <td><?php echo $postulados_v["cedula"] ?></td>
	            <td><?php echo $postulados_v["telefono"] ?></td>
                 <td  class="negrita"><?php echo $postulados_v["total2"]?></td>
                 <td><?php 
				 $porcentaje_votos= 100*$postulados_v["total2"]/$total_votos["total_votos"];
				 echo number_format( $porcentaje_votos, 1, ',', ' '); ?></td>
                 
                 
                 </tr>
                 
                

<?php }} ?>
 </tbody>
          <tfoot> 
         <h5 style="font-weight:bold; font-style:italic; text-align:center"> TOTAL VOTOS REGISTRADOS :  <?php echo $total_votos["total_votos"];?></h5> 
          </tfoot>       
                 </table>
                 
                 </div>
<!------------------------ 	GRAFICAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA  ---------------------------------->
<figure id="columnchart_values"  style="display:block;"></figure>

<span class="hora_reporte">  
Reporte Generado a las <?php echo  date(" H:i:s")?>
</span>




<div class="autor">
<p>SISTEMA AUTOMATIZADO PARA LA DEFENSA DEL VOTO<strong style="color:#900"> [SADEV 2.1] </strong>
	<br>	
   Desarrollado y Diseñado por ING.RAY SARACUAL </p>	
</div>

  <div align="center">
 <a class="btn" onclick="window.print()"><span class="glyphicon glyphicon-print"> IMPRIMIR</span></a>
</div>


<?php }else { ?>

<p align="center" style="padding:5%;"> NO SE HAN REGISTRADO POSTULACIONES PARA 
<?php if($_REQUEST["combo1"]==12){
	 echo $_SESSION['todas']; 
	  } else {
		   echo $_SESSION['p']; 
		  
		  
	  }
	     }?> </p>


</article>

</section>

</body>
</html>