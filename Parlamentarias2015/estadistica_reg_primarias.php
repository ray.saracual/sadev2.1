<?php 
$self = $_SERVER['PHP_SELF']; //Obtenemos la página en la que nos encontramos
header("refresh:60; url=$self"); //Refrescamos cada 300 segundos


include"../conexion/sesion.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>SADEV 2.1|Relacion de Registros </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" type="text/css" href="../dist/css/imprimir-estadistica.css" media="print"/>
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet"  href="../dist/css/bootstrap.css">
<link rel="stylesheet"  href="../jui/jquery-ui-1.11.2.custom/jquery-ui.theme.css">
<link rel="stylesheet"  href="../jui/jquery-ui-1.11.2.custom/jquery-ui.css">
<style>
	
		.datos_complementarios {
	margin-left:auto;
	margin-right:auto;
	box-shadow : rgba(0,0,0,0.3) 0px 0px 1em;	
	margin-bottom:2%;
	width:auto;
	padding:1%;


}
.membrete{
	display:none;
	
	}
	.titulos_centrados {
		display:none;
		
		}
		
		.hora_reporte {
			font-style:italic;
				font-weight: bold;
				text-align:right;
				font-size:18px;
								
								
								}
	

</style>
<!-- Latest compiled and minified JavaScript -->
<script src="../js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="../js/jsapi.js"></script>
     <script src="../dist/js/bootstrap.js"></script>
 <script src="../jui/jquery-ui-1.11.2.custom/jquery-ui.min.js"></script>
<?php 
include "../conexion/db.php";

// Conexión a la base de datos

$enlace = conectar();


 $postulados_general=mysql_query("SELECT *
 FROM tbl_candidaitosprimarias_votos",$enlace);
 
 

$totalRows_postulados_general = mysql_num_rows($postulados_general);
$total_regUbch_edo= $totalRows_postulados_general/12;
$total_regUbch_edo_porcentaje= ($total_regUbch_edo)*100/49;
$faltante= 49-$total_regUbch_edo;
$faltante_porcentaje=((49-$total_regUbch_edo)*100/49);
		 
$postulados_d = mysql_fetch_assoc($postulados_general)

?>

<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
             ['CLP Registradas',    <?php echo number_format ($total_regUbch_edo_porcentaje); ?>],
		        ['CLP por Registrar',  <?php echo number_format ($faltante_porcentaje); ?>    ],
         
        ]);

        var options = {
          title: 'GRAFICO RELACIÓN REGISTRO DE VOTOS',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>


     
</head>

<body>
<section>

<nav id="menu">

<?php include "../menu/menu_parlamentarias.php" ;


?>

</nav>
<header>
 <ol class="breadcrumb">
  <li><a href="#">Inicio</a></li>
  <li class="active">ESTADISTICA REGISTRO DE VOTOS PARA PRIMARIAS PSUV</li>
</ol>

 
  </header>
    
    
<article class="datos_complementarios">

<header class="membrete">


<img src="../images/Psuv-logo.png"  alt=""/>
<p>
REGISTRO DE VOTOS POR PARROQUIA  
<?php if( $_REQUEST["combo1"]==12){
	 echo $_SESSION['todas']; 
	  } else {
		   echo $_SESSION['p']; 
		  		  
	  }?> </li>
</p>
 <img src="../images/sala.png"  alt=""/>


</header>

 
<p class="titulo_iniciarS">ESTADISTICA REGISTRO DE VOTOS <br>
 </p>

 <table  width="90%" class="table table-bordered">
	    <thead style="font-size:18px;">
	        <tr>
            <th width="27%" height="41"><div align="center">TOTAL</div></th>
	            <th width="12%">49</th>
	            <th width="16%">
				<?php  
				       echo  $total_regUbch_edo;
				    ?></th>
	                       <th width="16%"><?php echo number_format($total_regUbch_edo_porcentaje); ?>%</th>
                                <th width="16%">
								       <?php 
									   echo $faltante; 		  
				                          ?></th>
                  <th width="13%"><?php echo number_format($faltante_porcentaje); ?>%</th>
	        </tr>
	    </thead>
        </table>

         <?php 
 
       
 $postulados=mysql_query("SELECT  pq.parroquia,pq.total_clp, COUNT(primarias.id_parroquia) AS total
		 FROM tbl_parroquias AS pq

LEFT JOIN   tbl_candidaitosprimarias_votos AS primarias  
ON  pq.id_parroquia = primarias.id_parroquia 

		GROUP BY pq.id_parroquia
		ORDER BY total DESC",$enlace);

$totalRows_postulados = mysql_num_rows ($postulados);
	 
//}
		 
	  if ($totalRows_postulados>0){
	    $cont=0;?>
	   
      
	<table width="90%" class="table table-bordered">
	    <thead style="font-size:15px;">
	        <tr>
	            <th>#</th>
                <th>PARROQUIA</th>
	            <th>TOTAL CLP</th>
	            <th>CLP REGISTRADOS</th>
	            <th>%CLP REGISTRADOS</th>
                 <th>CLP POR REGISTRAR</th>
                  <th>% CLP POR REGISTRAR</th>
	        </tr>
	    </thead>
       <?php
	 while ($postulados_d = mysql_fetch_assoc($postulados)){
		  
	   $cont++;
	   
	   ?>
        <tbody style="font-size:18px;">
       <?php 
	        if ($cont==1){ ?>
     <tr style=" background-color:#CCC;
      font-style:italic; font-weight:bold;">
            <?php } else { ?>
            <tr>
            <?php }?>
            
	            <td><?php echo $cont ?></td>
                <td><?php echo $postulados_d["parroquia"] ?></td>
	            <td><?php echo $postulados_d["total_clp"] ?></td>
	            <td><?php  $ubch_registradas_pq=$postulados_d["total"]/12; 
                      echo $ubch_registradas_pq 
					  ?></td>
	            <td><?php echo number_format ($ubch_registradas_pq*100/$postulados_d["total_clp"]) ?>%</td>
                 <td><?php $por_registrar = $postulados_d["total_clp"]-$ubch_registradas_pq;
				           echo $por_registrar;
				  ?></td>
                <td><?php echo number_format($por_registrar*100/$postulados_d["total_clp"])?>%</td>
	        </tr>
	     	       
	    </tbody>
        
        <?php 
		 }
		   ?>
	</table>



<figure id="piechart_3d"  style="width: 600px; height:200px; margin-left:auto; margin-right:auto;">
</figure>

<div class="quien_imprime">
<?php echo "Operador que imprimio : ". $_SESSION['usuario'];?>
</div>

<span class="hora_reporte">  
Reporte Generado a las <?php echo  date(" H:i:s")?>
</span>

<div class="autor">
<p>SISTEMA AUTOMATIZADO PARA LA DEFENSA DEL VOTO<strong style="color:#900"> [SADEV 2.1] </strong>
	<br>	
   Desarrollado y Diseñado por ING.RAY SARACUAL </p>	
</div>

<div align="center">
 <a class="btn" onclick="window.print()"><span class="glyphicon glyphicon-print"> IMPRIMIR</span></a>
</div>




<?php }else { ?>

<p align="center" style="padding:5%"> NO SE HAN REGISTRADO POSTULACIONES PARA ESTE CIRCULO  </p>

<?php }?>


</article>





</section>


</body>
</html>