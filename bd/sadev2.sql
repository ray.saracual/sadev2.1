-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 17-05-2018 a las 03:51:11
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sadev2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asamblea15_candidato`
--

CREATE TABLE IF NOT EXISTS `asamblea15_candidato` (
  `id_candidato` int(5) NOT NULL AUTO_INCREMENT,
  `candidato` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `nominal` int(5) NOT NULL,
  PRIMARY KEY (`id_candidato`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asamblea15_partido`
--

CREATE TABLE IF NOT EXISTS `asamblea15_partido` (
  `id_partido` int(5) NOT NULL AUTO_INCREMENT,
  `abreviatura` varchar(20) COLLATE ucs2_spanish2_ci NOT NULL,
  `partido` varchar(100) COLLATE ucs2_spanish2_ci NOT NULL,
  `nominal` int(5) NOT NULL,
  `lista` varchar(5) COLLATE ucs2_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_partido`)
) ENGINE=MyISAM  DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish2_ci AUTO_INCREMENT=29 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asamblea15_partido_candidato`
--

CREATE TABLE IF NOT EXISTS `asamblea15_partido_candidato` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_candidato` int(5) NOT NULL,
  `id_partido` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish2_ci AUTO_INCREMENT=48 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asamblea15_votosn`
--

CREATE TABLE IF NOT EXISTS `asamblea15_votosn` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_parroquia` int(5) NOT NULL,
  `cod_centro` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `mesa` int(5) NOT NULL,
  `nro_acta` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `id_candidato` int(5) NOT NULL,
  `id_partido` int(5) NOT NULL,
  `voto` int(10) NOT NULL,
  `nominal` int(5) NOT NULL,
  `usuario` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish2_ci AUTO_INCREMENT=3535 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `electores_cruce_2012`
--

CREATE TABLE IF NOT EXISTS `electores_cruce_2012` (
  `id_elector` int(11) NOT NULL,
  `id_parroquia` int(11) NOT NULL,
  `parroquia` varchar(100) NOT NULL,
  `cod` int(11) NOT NULL,
  `nombre_centro` varchar(100) NOT NULL,
  `nac` varchar(5) NOT NULL,
  `cedula` int(15) NOT NULL,
  `nombre_apellido` varchar(200) NOT NULL,
  `telefono_1` varchar(20) NOT NULL,
  `telefono_2` varchar(20) NOT NULL,
  `psuv` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presi_bp_centros_mesas_vargas`
--

CREATE TABLE IF NOT EXISTS `presi_bp_centros_mesas_vargas` (
  `id_centros_mesas` int(11) NOT NULL AUTO_INCREMENT,
  `id_centro_vargas` int(11) DEFAULT NULL,
  `cod_centro_electoral` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `centro_electoral` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion_centro_electoral` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `mesa` int(11) DEFAULT NULL,
  `electores` int(11) DEFAULT NULL,
  `desde` int(11) DEFAULT NULL,
  `hasta` int(11) DEFAULT NULL,
  `id_parroquia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_centros_mesas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='centros electorales de Vargas' AUTO_INCREMENT=600 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_candidaitosprimarias_votos`
--

CREATE TABLE IF NOT EXISTS `tbl_candidaitosprimarias_votos` (
  `id_candidatos_votos` int(5) NOT NULL AUTO_INCREMENT,
  `id_parroquia` int(5) NOT NULL,
  `cod_circulo` varchar(10) NOT NULL,
  `nombre_apellido` varchar(100) NOT NULL,
  `votos` int(6) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  PRIMARY KEY (`id_candidatos_votos`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=733 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_candidatos_postulados`
--

CREATE TABLE IF NOT EXISTS `tbl_candidatos_postulados` (
  `id_postulados` int(2) NOT NULL AUTO_INCREMENT,
  `nacionalidad` varchar(11) NOT NULL,
  `cedula` int(10) NOT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `nombre_apellido` varchar(100) NOT NULL,
  `id_parroquia` int(2) NOT NULL,
  `cod_circulo` varchar(15) NOT NULL,
  `cod_centro` varchar(15) NOT NULL,
  `votos` int(3) NOT NULL,
  `descripcion` varchar(5) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  PRIMARY KEY (`id_postulados`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=833 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_cargos_testigo`
--

CREATE TABLE IF NOT EXISTS `tbl_cargos_testigo` (
  `id_cargo` int(5) NOT NULL AUTO_INCREMENT,
  `cargo` varchar(100) NOT NULL,
  PRIMARY KEY (`id_cargo`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_centros_2014_clp`
--

CREATE TABLE IF NOT EXISTS `tbl_centros_2014_clp` (
  `id_centro` int(10) NOT NULL AUTO_INCREMENT,
  `id_parroquia` int(10) NOT NULL,
  `codigo_centro` int(20) NOT NULL,
  `nombre_centro` varchar(100) NOT NULL,
  `codigo_circulo` int(20) NOT NULL,
  PRIMARY KEY (`id_centro`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=203 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla ` tbl_centros_2015`
--

CREATE TABLE IF NOT EXISTS ` tbl_centros_2015` (
  `id_centro` int(10) NOT NULL AUTO_INCREMENT,
  `id_parroquia` int(10) NOT NULL,
  `cod_centro` int(20) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `mesa` int(5) NOT NULL,
  `electores` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_centro`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=203 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_circulos_de_lucha`
--

CREATE TABLE IF NOT EXISTS `tbl_circulos_de_lucha` (
  `id_circulo` int(10) NOT NULL AUTO_INCREMENT,
  `id_parroquia` int(10) NOT NULL,
  `codigo_circulo` int(20) NOT NULL,
  `nombre_circulo` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `mesas` varchar(5) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_circulo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='Se usa para almacenar datos de circulos de luchas' AUTO_INCREMENT=50 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_delegados`
--

CREATE TABLE IF NOT EXISTS `tbl_delegados` (
  `id_delegado` int(10) NOT NULL AUTO_INCREMENT,
  `nacionalidad` varchar(20) NOT NULL,
  `cedula` int(20) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `sexo` varchar(20) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `anillo` int(2) NOT NULL,
  PRIMARY KEY (`id_delegado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_jefe_circulo`
--

CREATE TABLE IF NOT EXISTS `tbl_jefe_circulo` (
  `id_jefe_circulo` int(10) NOT NULL AUTO_INCREMENT,
  `id_parroquia` int(10) NOT NULL,
  `codigo_circulo` varchar(20) NOT NULL,
  `sexo` varchar(10) NOT NULL,
  `nombre_apellido` varchar(200) NOT NULL,
  `nacionalidad` varchar(20) NOT NULL,
  `cedula` int(15) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `anillo` int(2) NOT NULL,
  PRIMARY KEY (`id_jefe_circulo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_jefe_psectoriales`
--

CREATE TABLE IF NOT EXISTS `tbl_jefe_psectoriales` (
  `id_jefe_psectorial` int(10) NOT NULL AUTO_INCREMENT,
  `id_parroquia` int(10) NOT NULL,
  `codigo_circulo` int(20) NOT NULL,
  `codigo_centro` int(20) NOT NULL,
  `id_p_sectorial` int(10) NOT NULL,
  `sexo` varchar(20) NOT NULL,
  `nombre_apellido` varchar(200) NOT NULL,
  `nacionalidad` varchar(10) NOT NULL,
  `cedula` int(20) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `anillo` varchar(2) NOT NULL,
  PRIMARY KEY (`id_jefe_psectorial`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2002 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_jefe_ubch`
--

CREATE TABLE IF NOT EXISTS `tbl_jefe_ubch` (
  `id_jefe_ubch` int(10) NOT NULL AUTO_INCREMENT,
  `id_parroquia` int(10) NOT NULL,
  `codigo_circulo` int(10) NOT NULL,
  `codigo_centro` int(10) NOT NULL,
  `sexo` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre_apellido` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `nacionalidad` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `cedula` int(20) NOT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `correo` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `usuario` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `anillo` int(2) NOT NULL,
  PRIMARY KEY (`id_jefe_ubch`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=209 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_parroquias`
--

CREATE TABLE IF NOT EXISTS `tbl_parroquias` (
  `id_parroquia` int(10) NOT NULL AUTO_INCREMENT,
  `parroquia` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `total_clp` int(5) NOT NULL,
  `total_ubch` int(23) NOT NULL,
  PRIMARY KEY (`id_parroquia`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_patrulleros_sec`
--

CREATE TABLE IF NOT EXISTS `tbl_patrulleros_sec` (
  `id_patrullero_sec` int(10) NOT NULL AUTO_INCREMENT,
  `id_parroquia` int(10) NOT NULL,
  `codigo_circulo` int(20) NOT NULL,
  `codigo_centro` int(10) NOT NULL,
  `id_p_sectorial` int(10) NOT NULL,
  `sexo` varchar(10) NOT NULL,
  `nombre_apellido` varchar(200) NOT NULL,
  `nacionalidad` varchar(20) NOT NULL,
  `cedula` int(15) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `anillo` varchar(2) NOT NULL,
  PRIMARY KEY (`id_patrullero_sec`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10042 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_psuv_2012`
--

CREATE TABLE IF NOT EXISTS `tbl_psuv_2012` (
  `id_psuv_vargas` int(11) NOT NULL AUTO_INCREMENT,
  `id_parroquia_psuv` int(11) NOT NULL,
  `nacionalidad` varchar(5) NOT NULL,
  `cedula_psuv` varchar(11) NOT NULL,
  `apellidos_nombres` varchar(100) NOT NULL,
  `direccion_psuv` varchar(255) NOT NULL,
  `telefono_1_psuv` varchar(20) NOT NULL,
  `telefono_2_psuv` varchar(20) NOT NULL,
  `nuevo_psuv` varchar(5) NOT NULL,
  `nuevo_psuv_2006` varchar(5) NOT NULL,
  `firmo` varchar(5) NOT NULL,
  PRIMARY KEY (`id_psuv_vargas`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=117810 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_p_sectoriales`
--

CREATE TABLE IF NOT EXISTS `tbl_p_sectoriales` (
  `id_p_sectorial` int(10) NOT NULL AUTO_INCREMENT,
  `p_sectorial` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_p_sectorial`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_testigos_asamblea`
--

CREATE TABLE IF NOT EXISTS `tbl_testigos_asamblea` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_parroquia` mediumint(10) NOT NULL,
  `cod_centro` int(20) NOT NULL,
  `mesa` int(5) NOT NULL,
  `id_cargo` int(5) NOT NULL,
  `nacionalidad` varchar(15) NOT NULL,
  `cedula` int(20) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1950 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(10) NOT NULL AUTO_INCREMENT,
  `autor` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `clave` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `admin` int(1) NOT NULL,
  `consulta` int(1) NOT NULL,
  `imprimir` int(1) NOT NULL,
  `estadisticas` int(1) NOT NULL,
  `eliminar` varchar(1) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=17 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
