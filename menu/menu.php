<style>
.menu {

	color:#CCC;	
	}
	.menu a{
		color:#FFF;
		
		}
	
</style>

<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
 <a class="navbar-brand" href="../Portal/portal.php"> 
 <img src="../images/sadev.png" width="135" height="30" 
  alt=""/>
  </a> 
  </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> REGISTRAR
          <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="../psuvVargas/reg_jefe_circulo.php">Jefes de Circulos</a></li>
            <li><a href="../psuvVargas/reg_jefe_ubch.php">Jefes de UBCH</a></li>
            <li><a href="../psuvVargas/reg_jefe_psectoriales.php" class="active">Jefe Patrullas Sectoriales</a></li>
             <li><a href="../psuvVargas/reg_p_t1.php" class="active">Patrulleros Sectoriales</a></li>
            
                    
          </ul>
          </li>
          
           <!--------------------- EDITAR O ADMINISTRAR UBCH--------------------------------------------------------->
 
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">ADMINISTRAR 
         
          <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
           <li><a href="../psuvVargas/adm_j_circulo.php"> Jefes de Circulo </a></li>
            <li><a href="../psuvVargas/select_edit_ubch.php">Jefe de UBCH y Jefes de Patrullas Sectoriales </a></li>
            <li><a href="../psuvVargas/buscar_p_sectorial.php">Buscar Patrulla Sectorial</a></li>
            
           </ul>
           
           </li>
          
          
           <!--------------------- DELEGADOS PSUV--------------------------------------------------------->
            <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">DELEGADOS PSUV
          <span class="caret"></span></a>
         
          <ul class="dropdown-menu" role="menu">
            <li><a href="../psuvVargas/reg_delegado_psuv.php">Registrar Delegado</a></li>
            <li><a href="../psuvVargas/adm_delegados.php">Administrar delegados</a></li>
            
           </ul></li>
          
          
          
          
         
           <!-- <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>-->
       
          
             <li><a href="../psuvVargas/listados.php">LISTADOS<span class="sr-only">(current)</span></a></li>
         <!-- <li><a href="#">Consutas<span class="sr-only">(current)</span></a></li>-->
        </li>
      </ul>
      <?php if ($_SESSION['consulta']==1){?>
      <form class="navbar-form navbar-left" role="search" action="../Portal/consulta_directa.php" method="post">
        <div class="form-group">
            <input type="number" class="form-control" placeholder="Consulta Directa" name="cedula" required>
        </div>
        <button type="submit" class="btn btn-default">Consultar</button>
      </form>
      <?php }?>
      <ul class="nav navbar-nav navbar-right">
      
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
          Bienvenido(a)<span class="caret"> </span></a>
          <ul class="dropdown-menu" role="menu">
            <!--<li><a href="#">Mi Perfil</a></li>
            <li><a href="#">Cambiar Contraseña</a></li>-->
           <li class="divider"></li>
           <li><a href="#">  <span class="glyphicon glyphicon-user"> </span> <?php echo $_SESSION['usuario']; ?></a></li>
           <li><a href="../psuvVargas/edit_usuario.php" ><span class="glyphicon glyphicon-wrench"> </span> Cambiar Contraseña</a></li>
              <li><a href="../index.php?cmd=cerrar"><span class="glyphicon glyphicon-off"> </span>  Cerrar Sesión </a></li>
              
               

          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>